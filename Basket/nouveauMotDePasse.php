<!doctype html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <title>Connexion</title>

  <link rel="canonical" href="https://getbootstrap.com/docs/4.5/examples/floating-labels/">

  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

  <style>
    .bd-placeholder-img {
      font-size: 1.125rem;
      text-anchor: middle;
      -webkit-user-select: none;
      -moz-user-select: none;
      -ms-user-select: none;
      user-select: none;
    }

    @media (min-width: 768px) {
      .bd-placeholder-img-lg {
        font-size: 3.5rem;
      }
    }
  </style>
  <link href="floating-labels.css" rel="stylesheet">
</head>

<body>
  <?php if(!isset($_POST['submit'])){
    ?>
    <form class="form-signin" action="<?php echo htmlentities($_SERVER['PHP_SELF']); ?>" method="post">
      <div class="text-center mb-4">
        <h2>Nouveau mot de passe</h2>
        <img class="mb-4" src="ballon.png" alt="" width="72" height="72">
        <h3 class="h3 mb-3 font-weight-normal">Changement mot de passe</h3>
      </div>
      <div class="form-label-group">
        <input type="email" id="inputEmail" class="form-control" placeholder="Email address" required autofocus name="mail">
        <label for="inputEmail">Email</label>
      </div>

      <div class="form-label-group">
        <input type="password" id="inputPassword" id="change" class="form-control" placeholder="Password" required name="pwd">
        <label for="inputPassword">Mot de passe</label>
      </div>

      <div class="form-label-group">
        <input type="password" id="inputPassword" id="change" class="form-control" placeholder="Password" required name="cpwd">
        <label for="inputPassword">Confirmer mot de passe</label>
      </div>

      <button class="btn btn-lg btn-primary btn-block" type="submit" name="submit">Sauvegarder le mot de passe</button>
    </form>


    <?php
  }else{
    try{
      $db = new PDO('sqlite:BDD.db');
      $code = $_POST['pwd'];
      $mail = $_POST['mail'];
      $statement = $db->query('SELECT COUNT(*) FROM UTILISATEUR WHERE mail="'.$mail.'"');
      $row = $statement->fetchAll(PDO::FETCH_COLUMN, 0);
      $res=intval($row[0]);
      if($_POST['pwd'] == $_POST['cpwd'] && $res!=0){


        $statement = $db->query('UPDATE UTILISATEUR SET motDePasse = "'.$code.'", mdpOublie = 0 WHERE mail = "'.$mail.'"');
        header('Location: vueConnexion.php');
        exit();
      }else{
        header('Location: nouveauMotdePasse.php');
        exit();
      }
    } catch(PDOException $e)
    {
    }
  }


  ?>
</body>
</html>
