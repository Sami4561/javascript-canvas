var longueur = 28;
var largeur = 15;
var lterrain = lt*0.9;
var hterrain = ht*0.85;
var touchelong = lt*0.05;
var touchelarg = ht*0.075;

var TeX = '';
var nom = '';


function exporterTex(){
    let nomFichier;
    nom = prompt("Veuillez entrer le nom du schéma", "Schéma1");
    if(nom == null) { return; }
    
    let i;
    
    TeX = TeX.concat('\\', 'begin{figure}\n');
    TeX = TeX.concat('\\', 'begin{center}\n');
    TeX = TeX.concat('\\', 'resizebox{.48\\linewidth}{!}{\n');
    TeX = TeX.concat('\\', 'begin{tikzpicture}\n');

    if(type_terrain == 0) {
        fullTerrain();
    }
    else if(type_terrain == 1) {
        demiTerrainBas();
    }
    else if(type_terrain == 2) {
        demiTerrainHaut();
    }

    
    TeX = TeX.concat('','\n');
    TeX = TeX.concat('\\', 'end{tikzpicture}\n}\n');
    TeX = TeX.concat('\\', 'end{center}\n');
    TeX = TeX.concat('\\', 'caption{');
    TeX = TeX.concat(nom, '}\n');
    TeX = TeX.concat('\\', 'end{figure}');
    nomFichier = nom + ".tex";
    download(nomFichier, TeX);
    TeX = '';
}

//Permet de trouver la position en longueur du terrain (vertical) en LaTeX
function findX(dim) {
    let ajust = dim - touchelong;
    let long2 = lterrain;
    if(type_terrain == 1){ 
        console.log("test"); 
        long2 = long; 
        longueur = 15;
        ajust = dim - l;
    }
    return Math.round(ajust*longueur/long2*100)/100;
}

//Permet de trouver la position en largeur du terrain (vertical) en LaTeX
function findY(dim) {
    let ajust = dim - touchelarg;
    let haut2 = hterrain;
    let neg = -1;
    if(type_terrain == 1){
    console.log("test");  
        haut2 = haut; 
        largeur = 14;
        ajust = dim - haut;
        ajust = ajust * neg + touchelarg;
    }
    return Math.round(ajust*largeur/haut2*100)/100;
}

/* FONCTIONS TYPE TERRAIN 2 */
// //Permet de trouver la position en longueur du terrain (vertical) en LaTeX
// function findX(dim) {
//     let ajust = dim - touchelong;
//     let long2 = lterrain;
//     if(type_terrain == 2){ 
//         long2 = long; 
//         longueur = 15;
//         ajust = dim - l;
//     }
//     return Math.round(ajust*longueur/long2*100)/100;
// }

// //Permet de trouver la position en largeur du terrain (vertical) en LaTeX
// function findY(dim) {
//     let ajust = dim - touchelarg;
//     let haut2 = hterrain;
//     if(type_terrain == 2){   
//         haut2 = haut; 
//         largeur = 14;
//         ajust = dim + haut - touchelarg;
//     }
//     return Math.round(ajust*largeur/haut2*100)/100;
// }

function download(filename, text) {
  var element = document.createElement('a');
  element.setAttribute('href', 'data:application/x-tex;charset=utf-8,' + encodeURIComponent(text));
  element.setAttribute('download', filename);

  element.style.display = 'none';
  document.body.appendChild(element);

  element.click();

  document.body.removeChild(element);
}

function fullTerrain(){
    TeX = TeX.concat('\n\n', '%%%Terrain');
    TeX = TeX.concat('\n', '\\draw (0,0) -- (0,28);');
    TeX = TeX.concat('\n', '\\draw (15,0) -- (15,28);');
    TeX = TeX.concat('\n', '\\draw (0,28) -- (15,28);');
    TeX = TeX.concat('\n', '\\draw (0,0) -- (15,0);');
    TeX = TeX.concat('\n', '\\draw (15,14) -- (0,14);');
    TeX = TeX.concat('\n', '\\draw  (7.5,14) circle [radius=2.4];;');
    TeX = TeX.concat('\n', '\\draw  (7.5,1.776) circle [radius=0.2];;');
    TeX = TeX.concat('\n', '\\draw  (6.5,1.575) -- (8.5,1.575);');
    TeX = TeX.concat('\n', '\\draw  (7.5,26.2) circle [radius=0.2];;');
    TeX = TeX.concat('\n', '\\draw  (6.5,26.425) -- (8.5,26.425);');
    TeX = TeX.concat('\n', '\\draw  (5,0) -- (5,5.8);');
    TeX = TeX.concat('\n', '\\draw  (10,0) -- (10,5.8);');
    TeX = TeX.concat('\n', '\\draw  (10,5.8) -- (5,5.8);;');
    TeX = TeX.concat('\n', '\\draw  (9.3,5.8) arc [radius=1.8, start angle=0, end angle= 180];');
    TeX = TeX.concat('\n', '\\draw  (10,28) -- (10,22.2);');
    TeX = TeX.concat('\n', '\\draw  (5,28) -- (5,22.2);');
    TeX = TeX.concat('\n', '\\draw  (5,22.2) -- (10,22.2);');
    TeX = TeX.concat('\n', '\\draw  (5.6,22.2) arc [radius=1.8, start angle=180, end angle= 360]; ');
    TeX = TeX.concat('\n', '\\draw  (14.1,1.7) arc [radius=6.6, start angle=0, end angle= 180];');
    TeX = TeX.concat('\n', '\\draw  (.9,0) -- (.9,1.7);');
    TeX = TeX.concat('\n', '\\draw  (14.1,0) -- (14.1,1.7);');
    TeX = TeX.concat('\n', '\\draw  (0.9,26.3) arc [radius=6.6, start angle=180, end angle= 360];');
    TeX = TeX.concat('\n', '\\draw  (.9,28) -- (.9,26.2);');
    TeX = TeX.concat('\n', '\\draw  (14.1,28) -- (14.1,26.2);');

    TeX = TeX.concat('\n\n', '%%%Zone d\'action');
    if(zone.length != 0) {
        TeX = TeX.concat('\n','\\fill[color=gray!20] ');
        for (i = 0; i < zone.length; i++) {
            TeX = TeX.concat('', '(');
            TeX = TeX.concat(findY(zone[i].y), ',');
            TeX = TeX.concat(findX(zone[i].x), ') -- ');
        }
        TeX = TeX.concat('','cycle;');
    }
    
    TeX = TeX.concat('\n\n', '%%%Joueurs\n');
    for (i = 0; i < joueurs.length; i++) {
        let z = joueurs[i];
        TeX = TeX.concat('\\','node[draw, circle] (');
        TeX = TeX.concat('', z.nom);
        TeX = TeX.concat('', ') at ');
        TeX = TeX.concat('(', findY(z.y));
        TeX = TeX.concat(', ', findX(z.x));
        TeX = TeX.concat(') {\\LARGE ', i+1);
        TeX = TeX.concat('', '};\n');
    }
    TeX = TeX.concat('\n', '%%%Adversaires\n');
    for (i = 0; i < adversaire.length; i++) {
        let r = adversaire[i];
        let tx = r.a + r.rayon*Math.cos(r.angle);
        let ty = r.b+ r.rayon *Math.sin(r.angle);
        let px = r.a + r.rayon*Math.cos(r.inverse);
        let py = r.b+ r.rayon *Math.sin(r.inverse);
        let rx = tx + r.rretour *Math.cos(r.rangle);
        let ry = ty+ r.rretour *Math.sin(r.rangle);
        let ox = px + r.rretour*Math.cos(r.rangle);
        let oy = py + r.rretour *Math.sin(r.rangle);
        TeX = TeX.concat('\n', '\\draw (');
        TeX = TeX.concat(findY(ty), ',');
        TeX = TeX.concat(findX(tx), ') -- (');
        TeX = TeX.concat(findY(ry), ',');
        TeX = TeX.concat(findX(rx), ');');
        TeX = TeX.concat('\n', '\\draw (');
        TeX = TeX.concat(findY(py), ',');
        TeX = TeX.concat(findX(px), ') -- (');
        TeX = TeX.concat(findY(oy), ',');
        TeX = TeX.concat(findX(ox), ');');
        TeX = TeX.concat('\n', '\\draw (');
        TeX = TeX.concat(findY(ty), ',');
        TeX = TeX.concat(findX(tx), ') -- (');
        TeX = TeX.concat(findY(py), ',');
        TeX = TeX.concat(findX(px), ');\n');
    }
    TeX = TeX.concat('\n', '%%%Passes\n');
    for (i = 0; i < passes.length; i++) {
        TeX = TeX.concat('\\', 'draw[thick,dashed] [-latex] (');
        TeX = TeX.concat(passes[i].x.nom, ') -- (');
        TeX = TeX.concat(passes[i].x2.nom, ');');
        TeX = TeX.concat('', '\n');
    }
    TeX = TeX.concat('\n', '%%%Déplacements/Écrans\n');
    for (i = 0; i < deple.length/4; i++) {
        let r = deple[3+i*4];
        if (r.ec == true){
            TeX = TeX.concat('\\', 'draw[thick] [-[] (' );
        } else {
            TeX = TeX.concat('\\', 'draw[thick] [-latex] (' );
        }
        TeX = TeX.concat(deple[0+i*4].jbase + 1, ') .. controls (');
        TeX = TeX.concat(findY(deple[1+i*4].y),', ');
        TeX = TeX.concat(findX(deple[1+i*4].x),') and ');
        TeX = TeX.concat('(', '');
        TeX = TeX.concat(findY(deple[2+i*4].y), ', ');
        TeX = TeX.concat(findX(deple[2+i*4].x), ') .. (');
        TeX = TeX.concat(findY(deple[3+i*4].y), ', ');
        TeX = TeX.concat(findX(deple[3+i*4].x), ');\n');

    }
    TeX = TeX.concat('\n', '%%%Dribbles\n');
    for (i = 0; i < dribblement.length/4; i++) {
        TeX = TeX.concat('\\', '\draw[thick, decoration = {zigzag}, decorate] [-latex] (' );
        TeX = TeX.concat(dribblement[0+i*4].dbase + 1, ') .. controls (');
        TeX = TeX.concat(findY(dribblement[1+i*4].y),', ');
        TeX = TeX.concat(findX(dribblement[1+i*4].x),') and ');
        TeX = TeX.concat('(', '');
        TeX = TeX.concat(findY(dribblement[2+i*4].y), ', ');
        TeX = TeX.concat(findX(dribblement[2+i*4].x), ') .. (');
        TeX = TeX.concat(findY(dribblement[3+i*4].y), ', ');
        TeX = TeX.concat(findX(dribblement[3+i*4].x), ');\n');
    }
    TeX = TeX.concat('\n', '%%%Tir\n');
    let t;
    for (i = 0; i < joueurs.length; i++) {
            let r = joueurs[i];
            if (r.tiring == true) {
                 t = r;
                 TeX = TeX.concat('\\', '\draw[thick, dashed] [-latex] (' );
                 TeX = TeX.concat(findY(t.y),',');
                 TeX = TeX.concat(findX(t.x),') -- (');
                 TeX = TeX.concat(findY(ht/2),',');
                 TeX = TeX.concat(findX(lt-lt*0.072),');\n');
            }
    }
}

function demiTerrainBas() {
    TeX = TeX.concat('\n\n', '%%%Terrain');
    TeX = TeX.concat('\n', '\\draw (0,0) -- (0,14);');
    TeX = TeX.concat('\n', '\\draw (15,0) -- (15,14);');
    TeX = TeX.concat('\n', '\\draw (0,14) -- (15,14);');
    TeX = TeX.concat('\n', '\\draw (0,0) -- (15,0);');
    TeX = TeX.concat('\n', '\\draw (15,14) -- (0,14);');
    TeX = TeX.concat('\n', '\\draw  (5,14) arc [radius=2.4, start angle=180, end angle= 360];');
    TeX = TeX.concat('\n', '\\draw  (7.5,1.776) circle [radius=0.2];;');
    TeX = TeX.concat('\n', '\\draw  (6.5,1.575) -- (8.5,1.575);');
    TeX = TeX.concat('\n', '\\draw  (5,0) -- (5,5.8);');
    TeX = TeX.concat('\n', '\\draw  (10,0) -- (10,5.8);');
    TeX = TeX.concat('\n', '\\draw  (10,5.8) -- (5,5.8);');
    TeX = TeX.concat('\n', '\\draw  (9.3,5.8) arc [radius=1.8, start angle=0, end angle= 180];');
    TeX = TeX.concat('\n', '\\draw  (14.1,1.7) arc [radius=6.6, start angle=0, end angle= 180];');
    TeX = TeX.concat('\n', '\\draw  (.9,0) -- (.9,1.7);');
    TeX = TeX.concat('\n', '\\draw  (14.1,0) -- (14.1,1.7);');

    TeX = TeX.concat('\n\n', '%%%Zone d\'action');
    if(zone.length != 0) {
        TeX = TeX.concat('\n','\\fill[color=gray!20] ');
        for (i = 0; i < zone.length; i++) {
            TeX = TeX.concat('', '(');
            TeX = TeX.concat(findX(zone[i].x), ',');
            TeX = TeX.concat(findY(zone[i].y), ') -- ');
        }
        TeX = TeX.concat('','cycle;');
    }

    TeX = TeX.concat('\n\n', '%%%Joueurs\n');
    for (i = 0; i < joueurs.length; i++) {
        let z = joueurs[i];
        TeX = TeX.concat('\\','node[draw, circle] (');
        TeX = TeX.concat('', z.nom);
        TeX = TeX.concat('', ') at ');
        TeX = TeX.concat('(', findX(z.x));
        TeX = TeX.concat(', ', findY(z.y));
        TeX = TeX.concat(') {\\LARGE ', i+1);
        TeX = TeX.concat('', '};\n');
    }
    TeX = TeX.concat('\n', '%%%Adversaires\n');
    for (i = 0; i < adversaire.length; i++) {
        let r = adversaire[i];
        let tx = r.a + r.rayon*Math.cos(r.angle);
        let ty = r.b+ r.rayon *Math.sin(r.angle);
        let px = r.a + r.rayon*Math.cos(r.inverse);
        let py = r.b+ r.rayon *Math.sin(r.inverse);
        let rx = tx + r.rretour *Math.cos(r.rangle);
        let ry = ty+ r.rretour *Math.sin(r.rangle);
        let ox = px + r.rretour*Math.cos(r.rangle);
        let oy = py + r.rretour *Math.sin(r.rangle);
        TeX = TeX.concat('\n', '\\draw (');
        TeX = TeX.concat(findX(ty), ',');
        TeX = TeX.concat(findY(tx), ') -- (');
        TeX = TeX.concat(findX(ry), ',');
        TeX = TeX.concat(findY(rx), ');');
        TeX = TeX.concat('\n', '\\draw (');
        TeX = TeX.concat(findX(py), ',');
        TeX = TeX.concat(findY(px), ') -- (');
        TeX = TeX.concat(findX(oy), ',');
        TeX = TeX.concat(findY(ox), ');');
        TeX = TeX.concat('\n', '\\draw (');
        TeX = TeX.concat(findX(ty), ',');
        TeX = TeX.concat(findY(tx), ') -- (');
        TeX = TeX.concat(findX(py), ',');
        TeX = TeX.concat(findY(px), ');\n');
    }
    TeX = TeX.concat('\n', '%%%Passes\n');
    for (i = 0; i < passes.length; i++) {
        TeX = TeX.concat('\\', 'draw[thick,dashed] [-latex] (');
        TeX = TeX.concat(passes[i].x.nom, ') -- (');
        TeX = TeX.concat(passes[i].x2.nom, ');');
        TeX = TeX.concat('', '\n');
    }
    TeX = TeX.concat('\n', '%%%Déplacements/Écrans\n');
    for (i = 0; i < deple.length/4; i++) {
        let r = deple[3+i*4];
        if (r.ec == true){
            TeX = TeX.concat('\\', 'draw[thick] [-[] (' );
        } else {
            TeX = TeX.concat('\\', 'draw[thick] [-latex] (' );
        }
        TeX = TeX.concat(deple[0+i*4].jbase + 1, ') .. controls (');
        TeX = TeX.concat(findX(deple[1+i*4].y),', ');
        TeX = TeX.concat(findY(deple[1+i*4].x),') and ');
        TeX = TeX.concat('(', '');
        TeX = TeX.concat(findX(deple[2+i*4].y), ', ');
        TeX = TeX.concat(findY(deple[2+i*4].x), ') .. (');
        TeX = TeX.concat(findX(deple[3+i*4].y), ', ');
        TeX = TeX.concat(findY(deple[3+i*4].x), ');\n');

    }
    TeX = TeX.concat('\n', '%%%Dribbles\n');
    for (i = 0; i < dribblement.length/4; i++) {
        TeX = TeX.concat('\\', '\draw[thick, decoration = {zigzag}, decorate] [-latex] (' );
        TeX = TeX.concat(dribblement[0+i*4].dbase + 1, ') .. controls (');
        TeX = TeX.concat(findX(dribblement[1+i*4].y),', ');
        TeX = TeX.concat(findY(dribblement[1+i*4].x),') and ');
        TeX = TeX.concat('(', '');
        TeX = TeX.concat(findX(dribblement[2+i*4].y), ', ');
        TeX = TeX.concat(findY(dribblement[2+i*4].x), ') .. (');
        TeX = TeX.concat(findX(dribblement[3+i*4].y), ', ');
        TeX = TeX.concat(findY(dribblement[3+i*4].x), ');\n');
    }
    TeX = TeX.concat('\n', '%%%Tir\n');
    let t;
    for (i = 0; i < joueurs.length; i++) {
            let r = joueurs[i];
            if (r.tiring == true) {
                 t = r;
                 TeX = TeX.concat('\\', '\draw[thick, dashed] [-latex] (' );
                 TeX = TeX.concat(findX(t.y),',');
                 TeX = TeX.concat(findY(t.x),') -- (');
                 TeX = TeX.concat(findX((l+0.44*long)+(0.12*long/2)),',');
                 TeX = TeX.concat(findY(haut),');\n');
            }
    }
}

function demiTerrainHaut(){


}
