<!DOCTYPE html>
<html lang="fr">
<head>
	<!-- Required meta tags -->
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

	<!-- Bootstrap CSS -->
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
	<link rel="stylesheet" href="basket.css">
	<title>Basket</title>
</head>
<body>
	<!-- Navbar -->
	<nav class="navbar navbar-expand-lg navbar-light" id="barre_nav">
		<div id="titre">
			<img id="image_ballon" src="ballon.png" width="30" height="30" alt="">
			<a class="navbar-brand text-white"  href="#">BASKET</a>
		</div>
		<button class="navbar-toggler bg-light" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
			<span class="navbar-toggler-icon"></span>
		</button>

		<div class="collapse navbar-collapse" id="navbarSupportedContent">
			<ul class="navbar-nav mr-auto">
				<li class="nav-item active">
					<a class="nav-link text-white" href="#">Connexion <span class="sr-only">(current)</span></a>
				</li>
				<li class="nav-item">
					<a class="nav-link text-white" href="#">Galerie</a>
				</li>
				<li class="nav-item dropdown">
					<a class="nav-link dropdown-toggle text-white" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
						Nouvelle page
					</a>
					<div class="dropdown-menu" aria-labelledby="navbarDropdown">
						<a class="dropdown-item" id="demo" href="#" onclick="afficherDemiTerrainDroit()">TERRAIN DROIT</a>
						<div class="dropdown-divider"></div>
						<a class="dropdown-item " href="#" onclick="afficherDemiTerrainGauche()">TERRAIN GAUCHE</a>
						<div class="dropdown-divider"></div>
						<a class="dropdown-item " href="#" onclick="afficherFullTerrain()" >FULL TERRAIN</a>
					</div>
				</li>
				<li class="nav-item dropdown">
					<a class="nav-link dropdown-toggle text-white" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
						Partager
					</a>
					<div class="dropdown-menu text-white" aria-labelledby="navbarDropdown">
						<a href="#" class="button dropdown-item" id="btn-download">PNG</a>
						<div class="dropdown-divider"></div>
						<script src="https://cdn.jsdelivr.net/npm/three@0.118.3/build/three.js"></script>
						<script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/1.5.3/jspdf.debug.js" integrity="sha384-NaWTHo/8YCBYJ59830LTz/P4aQZK1sS0SneOgAvhsIl3zBu8r9RevNg5lHCHAuQ/" crossorigin="anonymous"></script>
						<button class="dropdown-item" id="download">PDF</button>
						<div class="dropdown-divider"></div>
						<button class="dropdown-item" id="download" onclick="exporterTex()">TeX</button>
					</div>
				</li>
				<li class="nav-item">
					<a class="nav-link text-white" href="#">Sauvegarder</a>
				</li>
				<li class="nav-item dropdown">
					<a class="nav-link dropdown-toggle text-white" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
						Modifier
					</a>
					<div class="dropdown-menu text-white" aria-labelledby="navbarDropdown">
						<button class="button dropdown-item" onclick="togglePopup()">Attaquant</button>
						<div class="dropdown-divider"></div>
						<button class="button dropdown-item" onclick="togglePopup2()">Defenseur</button>
						<div class="dropdown-divider"></div>
						<button class="button dropdown-item" onclick="togglePopup3()">Terrain</button>
						<div class="dropdown-divider"></div>
						<button class="button dropdown-item" onclick="togglePopup4()">Centreinter</button>
						<div class="dropdown-divider"></div>
						<button class="button dropdown-item" onclick="togglePopup5()">Zone</button>
						<div class="dropdown-divider"></div>
						<button class="button dropdown-item" onclick="togglePopup6()">Mouvement</button>

					</div>
				</li>
				<li class="nav-item dropdown">
					<a class="nav-link dropdown-toggle text-white" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
						Taille
					</a>
					<div class="dropdown-menu text-white" aria-labelledby="navbarDropdown">
						<button class="button dropdown-item" onclick="togglePopupTa()">Attaquant</button>
						<div class="dropdown-divider"></div>
						<button class="button dropdown-item" onclick="togglePopup2Td()">Defenseur</button>

					</div>
				</li>
			</ul>
		</div>
	</nav>


	<?php
	$db = new PDO('sqlite:BDD.db');

	$appartient = $_GET['galerie'];

	$nbrPlan =current($db->query('SELECT COUNT(*) FROM PLAN where appartient="'.$appartient.'"')->fetch());

	$idPlan=$db->query('SELECT idPlan FROM PLAN WHERE appartient="'.$appartient.'"');
	$rows = $idPlan->fetchAll(PDO::FETCH_COLUMN, 0);



	?><table class="table table-hover"><?php
	for ($i=0; $i<=$nbrPlan-1; $i++) {
		$nomPlan=$db->query('SELECT nomPlan FROM PLAN WHERE idPlan="'.$rows[$i].'"');
		$row = $nomPlan->fetchAll(PDO::FETCH_COLUMN, 0);
		?><tr>
			<td>
				<h3 class="text-warning fs-1"><?php echo $i?></h3>
			</td>
			<td>
				<a class="nav-link text-warning" href="basket.php?idPlanCharge=<?=$rows[$i]?>&idgalerie=<?=$appartient?>&viensDeGalerie=1&nomPlan=<?=$row[0]?>"><?php echo $row[0]?></a>
			</td>
			</tr><?php
		}
		?></table><?php

		?>


		<!-- Optional JavaScript -->
		<!-- jQuery first, then Popper.js, then Bootstrap JS -->
		<script src="rectest.js"></script>
		<script src="latex.js"></script>
		<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
		<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
		<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.min.js" integrity="sha384-w1Q4orYjBQndcko6MimVbzY0tgp4pWB4lZ7lr30WKz0vr/aWKhXdBNmNb5D92v7s" crossorigin="anonymous"></script>


	</body>
	</html>