// get canvas related references
var canvas = document.getElementById("terrain");
zeubi = document.getElementById
var ctx = canvas.getContext("2d");
var le_contexte = canvas.getContext("2d");
var BB = canvas.getBoundingClientRect();
var offsetX = BB.left;
var offsetY = BB.top;
canvas.width = window.innerWidth*0.74;
var lt = canvas.width;
canvas.height = lt*0.536;
var ht = canvas.height;
var WIDTH = canvas.width;
var HEIGHT = canvas.height;
var buttonPNG = document.getElementById('btn-download');
var buttonPDF = document.getElementById('btn-downloadpdf');
var createtextbox = document.createElement("INPUT");
createtextbox.setAttribute("type", "text");
var taillejoueur=20;
var buttonAugmenter = document.getElementById('boutonatt+');
var buttonReduire = document.getElementById('boutonatt-');
var buttonAugmenterdef = document.getElementById('boutondef+');
var buttonReduiredef = document.getElementById('boutondef-');
var tailledefenseur = 20;
var nbIntervalle = 0;
var buttonAugmenterL = document.getElementById('boutonL+');
var buttonReduireL = document.getElementById('boutonL-');
var posxtouche=360,posytouche=20;
var posxligne=360,posyligne=20;
var posxmouv=360,posymouv=20;
var tailleligneP=5; 
var tailleligneN=2;
var type_terrain = 0;
var l = 0.3*lt;
var lmax = 0.75*lt;
var long = 0.45*lt;
var nbhig = 0;
var haut = long*0.93;

var r=255,g=0,b=0;
var r2=255,g2=0,b2=0;
var r3=255,g3=0,b3=0;
var r4=255,g4=0,b4=0;
var r5=255,g5=0,b5=0;
var r6=255,g6=0,b6=0;
//Variables qui stockeront la couleur en rgb.

var r_1=255,g_1=0,b_1=0;
var r_2=255,g_2=0,b_2=0;
var r_3=255,g_3=0,b_3=0;
var r_4=255,g_4=0,b_4=0;
var r_5=255,g_5=0,b_5=0;
var r_6=255,g_6=0,b_6=0;
var r7=255,g7=0,b7=0;
var r8=255,g8=0,b8=0;
var r_7=255,g_7=0,b_7=0;
var r_8=255,g_8=0,b_8=0;
// Variables secondaires rgb.
// drag related variables
var dragok = false;
var startX;
var startY;

//variable pour donner le ballon
var ball = false;
var nbball=0;
var nba = 0;
var nbb = 0;

//variable pour les tirs
var tirable = false;
var nbtir = 0;
//variable pour les zones d'influence
var nbzone = 0;
//array de points pour dessiner la zone d'influence
var zone = [];
var zonable = false;
var nbpoints = 0;

var effacing = false;
//variable pour editer zone d influence et mouvements
var edit = false;
var ecra = false;
//variable pour creer des deplacements
var bouge = false;
var nbptsdep = 0;
var currentdep = 0;
var deple  = [];
var nbbase = 0;
var deplacement = [];
var depdeu = [];
var nbdepdeu =0;
var usabledep = false;
var currentdepdeu =false;
var currentdepun = false;
var currentdrib = false;
var drideu = [];
var nbdrideu = 0;

var dribblement = [];
var nbptsdib = 0;
var drib = false;
var nbbased =0;
var nbdrib = 0;
var nombredepdeu = 0;
//variable pour faire une passe 
var nbjpass = 0;
var passable = false;
var nbclicpass = 0;
var dernierpass = null;
var currentdep = false;
// array de cercle de joueurs
var joueurs = [];
// array de cercle d'adversaire
var adversaire = [];
// array de passes
var passes = [];
// array de numero de joueur pour créér la passe
var jpass = [];
// array d'action pour pouvoir supprimer la derniere 
var last = [];

var nbhig = 0;
// listen for mouse events
canvas.onmousedown = myDown;
canvas.onmouseup = myUp;
canvas.onmousemove = myMove;

// call to draw the scene
draw();

// draw a single rect
function rect(x, y, w, h) {
    ctx.beginPath();
    ctx.rect(x, y, w, h);
    ctx.strokeStyle =document.getElementById("resultatmouv").value;
    ctx.closePath();
    ctx.fill();
}

function cercle(x,y,z,a,b,c,g,i) {

    ctx.beginPath();
    ctx.fillStyle = g;
    ctx.arc(x,y,z,a,b,c);
    console.log(r_1);
    console.log(g_1);
    console.log(b_1);
    if(r_1 < 99 && g_1 < 99 && b_1 < 99){
        ctx.strokeStyle = "white";
    }
    else{
        console.log("zzz");
        ctx.strokeStyle = "black";
    }
    if(taillejoueur < 20 )
        ctx.font = "15px serif";
    else
        ctx.font = "20px serif";

    ctx.fill();
    ctx.setLineDash([]);
    ctx.stroke();
    ctx.textBaseline = "hanging";
    ctx.strokeText(i, x-5, y-5);
    ctx.closePath();
}

function cercley(x,y,z,a,b,c,g,i) {

    ctx.beginPath();
    ctx.fillStyle = g;
    ctx.arc(x,y,z,a,b,c);
    ctx.strokeStyle =document.getElementById("resultatmouv").value;
    ctx.fill();
    ctx.setLineDash([]);
    ctx.stroke();
    ctx.closePath();
}

function cercleedit(x,y,z,a,b,c,g) {
    le_contexte.fillStyle = g;
    le_contexte.beginPath();
    le_contexte.arc(x,y,z,a,b,c);
    le_contexte.stroke();
    le_contexte.fill();
}

function lignePasse(a,b,c,d){
    var dx = c - a;
    var dy = d - b;
    var angle = Math.atan2(dy, dx);
    ctx.lineWidth = 2;
    ctx.beginPath();
    ctx.setLineDash([ 7 , 7]);
    ctx.strokeStyle =document.getElementById("resultatmouv").value;
    ctx.moveTo(a + taillejoueur * Math.cos(angle) , b + taillejoueur * Math.sin(angle));
    ctx.lineTo(c - taillejoueur * Math.cos(angle), d - taillejoueur * Math.sin(angle));
    ctx.stroke();
    canvas_arrow(ctx, a, b, c, d);
}

function ligneDrible(xa, ya, xb, yb){
    var intervalles = intervalleDroite(xa, ya, xb, yb);
    centreIntervalle(xa, ya, xb, yb, nbIntervalle, intervalles,false);
}

//Fonction permettant de dessiner la tête d'une flèche
function canvas_arrow(context, fromx, fromy, tox, toy) {
    var headlen = 15; // length of head in pixels
    var dx = tox - fromx;
    context.lineWidth = 2;
    var dy = toy - fromy;
    var angle = Math.atan2(dy, dx);
    context.beginPath();
    context.fillStyle ="black";
    context.setLineDash([]);
    context.moveTo(tox - taillejoueur * Math.cos(angle), toy - taillejoueur * Math.sin(angle));
    context.lineTo(tox - taillejoueur * Math.cos(angle) - headlen * Math.cos(angle - Math.PI / 10), toy - taillejoueur * Math.sin(angle) - headlen * Math.sin(angle - Math.PI / 10));
    context.moveTo(tox - taillejoueur * Math.cos(angle), toy - taillejoueur * Math.sin(angle));
    context.lineTo(tox - taillejoueur * Math.cos(angle) - headlen * Math.cos(angle + Math.PI / 10), toy - taillejoueur * Math.sin(angle) - headlen * Math.sin(angle + Math.PI / 10));
    context.lineTo(tox - taillejoueur * Math.cos(angle) - headlen * Math.cos(angle - Math.PI / 10), toy - taillejoueur * Math.sin(angle) - headlen * Math.sin(angle - Math.PI / 10))
    context.stroke();
    context.fill();
    context.closePath();
}

function canvas_arrow2(context, fromx, fromy, tox, toy) {
    var headlen = 15; // length of head in pixels
    context.lineWidth = 2;
    var dx = tox - fromx;
    var dy = toy - fromy;
    var angle = Math.atan2(dy, dx);
    context.beginPath();
    ctx.fillStyle ="black";
    context.setLineDash([]);
    context.moveTo(tox, toy);
    context.lineTo(tox - headlen * Math.cos(angle - Math.PI / 10), toy - headlen * Math.sin(angle - Math.PI / 10));
    context.moveTo(tox, toy);
    context.lineTo(tox - headlen * Math.cos(angle + Math.PI / 10), toy - headlen * Math.sin(angle + Math.PI / 10));
    context.lineTo(tox - headlen * Math.cos(angle - Math.PI / 10), toy - headlen * Math.sin(angle - Math.PI / 10));
    context.fill();
    context.stroke();
    context.closePath();
}

function ligne(a,b,c,d){
    ctx.beginPath();
    ctx.lineWidth = 2;
    ctx.setLineDash([]);
    ctx.strokeStyle =document.getElementById("resultatmouv").value;
    ctx.moveTo(a, b);
    ctx.lineTo(c, d);
    ctx.stroke();
}

function ligneappui(a,b,c,d,hi){

    le_contexte.beginPath();
    if(hi == true){
        ctx.lineWidth = 5;
    }
    else{
        ctx.lineWidth = 2;
    }
    le_contexte.setLineDash([]);
    le_contexte.strokeStyle = document.getElementById("resultatdef").value;
    le_contexte.moveTo(a, b);
    le_contexte.lineTo(c, d);
    le_contexte.stroke();
}

function ligneTir(a,b,c,d){
    ctx.beginPath();
    ctx.lineWidth = 2;
    ctx.setLineDash([2,5]);
    ctx.strokeStyle =document.getElementById("resultatmouv").value;
    ctx.moveTo(a, b);
    ctx.lineTo(c, d);
    ctx.stroke();
}

// clear the canvas
function clear() {
    ctx.clearRect(0, 0, WIDTH, HEIGHT);
}

// redraw the scene
function draw() {
    if(bouge || drib){

    }
    else{
        clear();
   // console.log("je dessine");
   ctx.fillStyle = "#FAF7F8";
   rect(0, 0, WIDTH, HEIGHT);
   if(type_terrain == 0){
    faireterrain();
} 
else if(type_terrain == 1){
    faireDemiTerrainBas();
} 
else if(type_terrain == 2){
    faireDemiTerrainHaut();
}
    // redraw each rect in the rects[] array
    if(depdeu.length > 0 && depdeu.length%2 ==0  && nombredepdeu>0 && currentdep == false){
        depladeu();
    }
    if(drideu.length > 0 && drideu.length%2 ==0){
        dribdeu();
    }
    if(dribblement.length%4 == 0 && dribblement.length>0){
     dribblage(); 
 }
 if(deple.length > 0 && deple.length%4 ==0){
    console.log("je dessine les deplacements");
    deplacer();
}
if(nbzone == 1){
    zoner();
}
for (var i = 0; i < joueurs.length; i++) {
    var r = joueurs[i];
    if(r.tiring == true){
        if(type_terrain == 0) {
            ligneTir(r.x,r.y,lt-lt*0.072-20, ht/2);
        }
        else if(type_terrain == 1) {
            ligneTir(r.x,r.y,(l+0.44*long)+(0.12*long/2), haut);
        } 
        else if(type_terrain == 2) {
            ligneTir(r.x,r.y,(l+0.44*long)+(0.12*long/2), lt*0.098-ht*0.075);
        }
    }
    if(r.gottheball){
        r.fill = "green";
    }
    else{
        r.fill = document.getElementById("resultat").value ;
    }
    ctx.fillStyle = r.fill;
    cercle(r.x,r.y,r.rayon,r.cache,r.taille,r.last,r.fill,r.nom);
}
for (var i = 0; i < adversaire.length; i++) {
    var r = adversaire[i];
    if(r.setball == true){
        r.fill = "green";
    }
    tx = r.x + r.rayon*Math.cos(r.angle);
    ty = r.y+ r.rayon *Math.sin(r.angle);
    px = r.x + r.rayon*Math.cos(r.inverse);
    py = r.y+ r.rayon *Math.sin(r.inverse);
    rx = tx - r.rretour *Math.cos(r.rangle);
    ry = ty- r.rretour *Math.sin(r.rangle);
    ox = px - r.rretour*Math.cos(r.rangle);
    oy = py - r.rretour *Math.sin(r.rangle);

    ctx.fillStyle = r.fill;
    ligneappui(tx,ty,rx,ry,r.hig);
    ligneappui(px,py,ox,oy,r.hig);
    ligneappui(tx,ty,px,py,r.hig);

}
for (var i = 0; i < jpass.length-1; i=i+2) {
    var r = jpass[i];
    var z = jpass[i+1];
    nbjpass=0;
    fairepasse(r,z);
}
for(var i = 0; i < passes.length; i++) {
    var r = passes[i];
    lignePasse(r.x.x,r.x.y,r.x2.x,r.y2.y);
}
}
}


// handle mousedown events
function myDown(e) {

    // tell the browser we're handling this mouse event
    e.preventDefault();
    e.stopPropagation();

    // get the current mouse position
    var mx = parseInt(e.clientX - offsetX);
    var my = parseInt(e.clientY - offsetY);
    //console.log(mx);
    //console.log(my);

    if(ball == true){
        dragok = false;
        for (var i = 0; i < joueurs.length; i++) {
            var r = joueurs[i];
            if (mx > r.x - r.rayon && mx < r.x + r.rayon && my > r.y - r.rayon && my < r.y + r.rayon) {
                // if yes, set that rects isDragging=true
                document.body.style.cursor = 'pointer';
                dragok = true;
                if (nbball == 0){
                    r.setball = true;
                    r.gottheball = true;
                    nbball = nbball + 1;
                    last.push({
                        action : 3,
                    });
                    untoggleDonner();
                }
                else 
                   window.alert("Un seul ballon sur le terrain");
               ball = false;
               draw();
           }
       }
       untoggleDonner();
       ball = false;
   }
   if(edit == true){
    dragok = false;
    for (var i = 0; i < zone.length; i++) {
        var r = zone[i];
        if (mx > r.x - r.rayon && mx < r.x + r.rayon && my > r.y - r.rayon && my < r.y + r.rayon) {
            document.body.style.cursor = 'pointer';
            dragok = true;
            r.isDragging = true;
        }
    }

    for (var i = 0; i < adversaire.length; i++) {
        var r = adversaire[i];
        if (mx > r.x - r.rayon && mx < r.x + r.rayon && my > r.y - r.rayon && my < r.y + r.rayon) {
                // if yes, set that rects isDragging=true
                document.body.style.cursor = 'pointer';
                if(r.hig == true){
                    nbhig = nbhig -1;
                    r.hig = false; 
                    if(nbhig == 0){
                        UntoggleOutilAppui();
                    }
                }
                else{
                    nbhig = nbhig+1;
                    r.hig = true; 
                    toggleOutilAppui();
                }
                draw();
            }

        }
        for (var i = 0; i < deple.length; i++) {
            var r = deple[i];
            if (mx > r.x - r.rayon && mx < r.x + r.rayon && my > r.y - r.rayon && my < r.y + r.rayon) {
                document.body.style.cursor = 'pointer';
                dragok = true;
                r.isDragging = true;
            }
        }
        for(var i = 0;i<deple.length/4;i++){
            var r = deple[3+i*4];
            if (mx > r.x - r.rayon && mx < r.x + r.rayon && my > r.y - r.rayon && my < r.y + r.rayon) {
                if(r.hig == true){
                    r.hig = false;
                    UntoggleOutilAppui();
                }
                else{
                    r.hig = true;
                    if(r.ec)
                        toggleOutilAppui();
                }
                
                draw();
            }
        }

        for (var i = 0; i < depdeu.length; i++) {
            var r = depdeu[i];
            if (mx > r.x - r.rayon && mx < r.x + r.rayon && my > r.y - r.rayon && my < r.y + r.rayon) {
                document.body.style.cursor = 'pointer';
                dragok = true;
                r.isDragging = true;
            }
        }
        for (var i = 0; i < drideu.length; i++) {
            var r = drideu[i];
            if (mx > r.x - r.rayon && mx < r.x + r.rayon && my > r.y - r.rayon && my < r.y + r.rayon) {
                document.body.style.cursor = 'pointer';
                dragok = true;
                r.isDragging = true;
            }
        }

        for (var i = 0; i < depdeu.length/2; i++) {
            var r = depdeu[1+i*2];
            if (mx > r.x - r.rayon && mx < r.x + r.rayon && my > r.y - r.rayon && my < r.y + r.rayon) {
                if(r.hig == true){
                    r.hig = false;
                    UntoggleOutilAppui();
                }
                else{
                    r.hig = true;
                    if(r.ec)
                        toggleOutilAppui();
                }
                
                draw();
            }
        }

        for (var i = 0; i < dribblement.length; i++) {
            var r = dribblement[i];
            if (mx > r.x - r.rayon && mx < r.x + r.rayon && my > r.y - r.rayon && my < r.y + r.rayon) {
                document.body.style.cursor = 'pointer';
                dragok = true;
                r.isDragging = true;
            }
        }
    }


    if(ecra == true){
        for(h = 0;h< (nbptsdep/4);h++){ 
            var r = deple[3+h*4];
            if (mx > r.x - 2*r.rayon && mx < r.x + 2*r.rayon && my > r.y - 2*r.rayon && my < r.y + 2*r.rayon) {
                if(r.ec){
                    r.ec = false;
                }
                else
                   r.ec = true;
               ecra = false;
               untoggleecran();
               draw();
           }
       }

       for(h = 0;h< (nbdepdeu/2);h++){ 
        var r = depdeu[1+h*2];
        if (mx > r.x - 2*r.rayon && mx < r.x + 2*r.rayon && my > r.y - 2*r.rayon && my < r.y + 2*r.rayon) {
            if(r.ec){
                r.ec = false;
            }
            else
               r.ec = true;
           ecra = false;
           untoggleecran();
           draw();
       }
   }
}



if(bouge == true){
    if(nbptsdep%4 == 0){
        for (var i = 0; i < joueurs.length; i++) {
            var r = joueurs[i];
            if (mx > r.x - r.rayon && mx < r.x + r.rayon && my > r.y - r.rayon && my < r.y + r.rayon) {
                if(r.setball || r.sedeplace){
                    window.alert(" ce joueur a le ballon il peut seulement dribbler");
                    bouge = false;
                    untoggleDeplacer();
                }
                else{
                    currentdep = true;
                    document.body.style.cursor = 'pointer';
                    deple.push({
                        x : joueurs[i].x,
                        y: joueurs[i].y,
                        edit : false,
                        rayon : joueurs[i].rayon,
                        jbase : i,
                    });
                    depdeu.push({
                        x : joueurs[i].x,
                        y: joueurs[i].y,
                        edit : false,
                        rayon : joueurs[i].rayon,
                        jbase : i,
                    });
                    r.sedeplace = true,
                    nbptsdep = nbptsdep +1;
                    nbbase = nbbase+1;
                    nbdepdeu = nbdepdeu +1;

                }

            }
        }
        if(nbbase == 0){
            currentdep = false;
            if(bouge)
                window.alert("Un deplacement doit partir d'un joueur");
            bouge = false;
            draw();
            untoggleDeplacer();
        }
    }


    else{
        g = mx;
        f = my;
        deple.push({
            x : g,
            y : f,
            edit : false,
            rayon : 15,
            isDragging : false,
            ec :false,
            hig : false,
            angle : Math.PI/2,
            inverse : -Math.PI/2,
        });
        depdeu.push({
            x : g,
            y : f,
            edit : false,
            rayon : 15,
            isDragging : false,
            ec : false,
            hig : false,
            angle : Math.PI/2,
            inverse : -Math.PI/2,
        });
        nbdepdeu = nbdepdeu +1;

        nbptsdep = nbptsdep +1;
        cercley(mx,my,3,0,Math.PI*2,false,"black");
        if(nbptsdep%4 == 0){
           currentdepun = true;
           deplace();
       }
   }
}
if(drib == true){
    dragok = false;
    if(nbptsdib%4 == 0){
        for (var i = 0; i < joueurs.length; i++) {
            var r = joueurs[i];
            if (mx > r.x - r.rayon && mx < r.x + r.rayon && my > r.y - r.rayon && my < r.y + r.rayon) {
                if(r.dribbb){
                    window.alert("Seul un joueur qui a le ballon peu dribbler");
                    drib = false;
                    untoggleDribble();  
                }
                else{
                    if(r.setball){
                        document.body.style.cursor = 'pointer';
                        dribblement.push({
                            x : joueurs[i].x,
                            y: joueurs[i].y,
                            edit : false,
                            rayon : joueurs[i].rayon,
                            dbase : i,
                        });
                        drideu.push({
                            x : joueurs[i].x,
                            y: joueurs[i].y,
                            edit : false,
                            rayon : joueurs[i].rayon,
                            dbase : i,
                        });
                        nbdrideu = nbdrideu+1;
                        r.dribbb = true,
                        nbptsdib = nbptsdib +1;
                        nbbased = nbbased+1;

                    }

                    if(r.setball == false){
                        if(drib)
                            window.alert("Seul un joueur qui a le ballon peu dribbler");
                        drib = false;
                        draw();
                        untoggleDribble();

                    }
                }
            }
        }
        if(nbbased == 0 && drib == true){
            if(drib){
                window.alert("Un dribble doit partir d'un joueur");
            }
            drib = false;
            draw();
            untoggleDribble();
        }
    }


    else{
        g = mx;
        f = my;
        dribblement.push({
            x : g,
            y : f,
            edit : false,
            rayon : 15,
            isDragging : false,
        });
        drideu.push({
            x : g,
            y : f,
            edit : false,
            rayon : 15,
            isDragging : false,
        });
        nbdrideu = nbdrideu +1;
        nbptsdib = nbptsdib +1;
        cercley(mx,my,3,0,Math.PI*2,false,"black");
        if(nbptsdib%4 == 0){
            currentdrib = true;
            dribble();
        }
    }
}


if(zonable == true){
    if(nbzone == 0){
        g = mx;
        f = my;
        zone.push({

            x : g,
            y : f,
            edit : false,
            rayon : 15,
            isDragging : false,
        });
        nbpoints=nbpoints+1;
        cercley(g,f,3,0,Math.PI*2,false,"black");
    }
    else{
        window.alert("Une seule zone d'influence sur le terrain");
        zonable = false;
        untogglezone();
    }
}
if(effacing == true){
    for(var i = 0; i < zone.length; i++){
        var r = zone[i];
        if (mx > r.x - r.rayon && mx < r.x + r.rayon && my > r.y - r.rayon && my < r.y + r.rayon) {
            while(zone.length != 0){
                zone.pop();
            }
            nbzone = 0;
            nbpoints = 0;
            draw();
        }
    }

    for (var i; i<joueurs.length;i++){
        var r = joueurs[i];
        if (mx > r.x - r.rayon && mx < r.x + r.rayon && my > r.y - r.rayon && my < r.y + r.rayon) {
            if(r.gottheball){
                nbball =0;
            }
            if(r.sedeplace){
                window.alert("On ne peut pas supprimer un joueur qui se déplace (pas eu le temps)");
                return;
            }
            if(r.dribbb){
                window.alert("On ne peut pas supprimer un joueur qui dribble (pas eu le temps)");
                return;
            }
            for(var j =0;j<passes.length;j++){
                var e = passes[j];
                if(e.x.nom != i && e.x2.nom != i ){
                    window.alert("On ne peut pas supprimer un joueur qui passe (pas eu le temps)");
                    return;
                }
            }
            if(r.tiring){
                nbtir =0;
            }

            else{
                joueurs.splice(i,1);
                draw();
            }
        }
    }

    for (var i; i<adversaire.length;i++){
        var r = adversaire[i];
        if (mx > r.x - r.rayon && mx < r.x + r.rayon && my > r.y - r.rayon && my < r.y + r.rayon) {
            adversaire.splice(i,1);
            draw();
        }
    }
}

if(passable == true ){
    nbclicpass = nbclicpass + 1;
    for (var i = 0; i < joueurs.length; i++) {
        var r = joueurs[i];
        if (mx > r.x - r.rayon && mx < r.x + r.rayon && my > r.y - r.rayon && my < r.y + r.rayon) {
                // if yes, set that rects isDragging=true
                document.body.style.cursor = 'pointer';
                if (nbjpass % 2 == 0){
                    if(r.setball == false){
                        passable = false;
                        nbjpass = 0;
                        window.alert("Seulement un joueur qui possede le ballon a un moment peut faire une passe");
                        document.body.style.cursor = 'default';
                        nbclicpass = 0;
                        untogglePasse();

                    }
                    else
                    {
                        dernierpass = i;
                        addj(i);
                        nbjpass=nbjpass+1;
                    }
                }
                else{
                    if(i == jpass[jpass.length -1].c){
                        window.alert("On ne peut se faire une passe a soit meme");
                        passable = false;
                        jpass.pop();
                        nbclicpass = 0;
                        untogglePasse();
                        nbjpass = 0;
                    }
                    else{
                        joueurs[dernierpass].setball = false;
                        r.setball = true;
                        dernierpass = i;

                        addj(i);
                        nbjpass=nbjpass+1;
                    }
                } 
                if(nbjpass % 2 == 0){
                    passable=false;
                    nbclicpass = 0;
                    untogglePasse();
                    draw();
                }
            }
        }
        if(nbjpass == 0){
            untogglePasse();
            passable = false;
            nbclicpass = 0;

        }
        if(nbjpass == 1 && nbclicpass == 2){
            jpass.pop();
            untogglePasse();
            passable = false;
            nbclicpass = 0;
            nbjpass = 0;
        }
    }
    if(tirable == true){
       for (var i = 0; i < joueurs.length; i++) {
        var r = joueurs[i];
        if (mx > r.x - r.rayon && mx < r.x + r.rayon && my > r.y - r.rayon && my < r.y + r.rayon) {
            if(r.setball == true){
                r.tiring = true;
                tirable = false;
                nbtir = nbtir +1;

                last.push({
                    action : 9,
                });
                draw();
            }
            else{
                tirable = false;
                window.alert("Seul un joueur qui a le ballon peut tirer");
            }
        }
    }
    untoggletire();
}

else {
    if(edit == false && drib == false){
        for (var i = 0; i < joueurs.length; i++) {
            var r = joueurs[i];
            if (mx > r.x - r.rayon && mx < r.x + r.rayon && my > r.y - r.rayon && my < r.y + r.rayon) {
                // if yes, set that rects isDragging=true
                document.body.style.cursor = 'pointer';
                if(drib == false){
                    dragok = true;
                    r.isDragging = true;
                }
            }
        }
        for (var i = 0; i < adversaire.length; i++) {
            var r = adversaire[i];
            if (mx > r.x - r.rayon && mx < r.x + r.rayon && my > r.y - r.rayon && my < r.y + r.rayon) {
                // if yes, set that rects isDragging=true
                document.body.style.cursor = 'pointer';
                dragok = true;
                r.isDragging = true;
            }
        }
        if(deple.length%4 == 0){
            for(var i = 0;i<deple.length/4;i++){
                var r = deple[3+i*4];
                if (mx > r.x - r.rayon && mx < r.x + r.rayon && my > r.y - r.rayon && my < r.y + r.rayon) {
                    document.body.style.cursor = 'pointer';
                    dragok = true;
                    r.isDragging = true;
                }
            }
        }

        if(depdeu.length%2 == 0){
            for (var i = 0; i < depdeu.length/2; i++) {
                var r = depdeu[1+i*2];
                if (mx > r.x - r.rayon && mx < r.x + r.rayon && my > r.y - r.rayon && my < r.y + r.rayon) {
                    document.body.style.cursor = 'pointer';
                    dragok = true;
                    r.isDragging = true;
                }
            }
        }
        if(dribblement.length%4 == 0){
            for(var i = 0;i<dribblement.length/4;i++){
                var r = dribblement[3+i*4];
                if (mx > r.x - r.rayon && mx < r.x + r.rayon && my > r.y - r.rayon && my < r.y + r.rayon) {
                    document.body.style.cursor = 'pointer';
                    dragok = true;
                    r.isDragging = true;
                }
            }
        }
        if(drideu.length%2 == 0){
            for (var i = 0; i < drideu.length/2; i++) {
                var r = drideu[1+i*2];
                if (mx > r.x - r.rayon && mx < r.x + r.rayon && my > r.y - r.rayon && my < r.y + r.rayon) {
                    document.body.style.cursor = 'pointer';
                    dragok = true;
                    r.isDragging = true;
                }
            }
        }
    }
}
console.clear();
console.log(deple);
console.log(" nbDeple : " + nbptsdep);

console.log(depdeu);
console.log(" nbdepdeu : " + nbdepdeu);

console.log(dribblement);
console.log(" nbDrible : " + nbptsdib);

console.log(drideu);
console.log(" nbdrideu : " + nbdrideu);



    // save the current mouse position
    startX = mx;
    startY = my;
}


// handle mouseup events
function myUp(e) {  
    // tell the browser we're handling this mouse event
    e.preventDefault();
    e.stopPropagation();

    // clear all the dragging flags
    document.body.style.cursor = 'default';
    dragok = false;
    for (var i = 0; i < joueurs.length; i++) {
        joueurs[i].isDragging = false;
    }
    for (var i = 0; i < adversaire.length; i++) {
        adversaire[i].isDragging = false;
    }
    for (var i = 0; i < zone.length; i++) {
        zone[i].isDragging = false;
    }
    for (var i = 0; i < deple.length; i++) {
        deple[i].isDragging = false;
    }
    for (var i = 0; i < depdeu.length; i++){
    	depdeu[i].isDragging = false;
    }
    for (var i = 0; i < dribblement.length; i++) {
        dribblement[i].isDragging = false;
    }
    for (var i = 0; i < drideu.length; i++) {
        drideu[i].isDragging = false;
    }
    console.log(joueurs);
}


// handle mouse moves
function myMove(e) {

    // if we're dragging anything...
    if (dragok) {

        // tell the browser we're handling this mouse event
        e.preventDefault();
        e.stopPropagation();

        // get the current mouse position
        var mx = parseInt(e.clientX - offsetX);
        var my = parseInt(e.clientY - offsetY);

        // calculate the distance the mouse has moved
        // since the last mousemove
        var dx = mx - startX;
        var dy = my - startY;

        // move each rect that isDragging 
        // by the distance the mouse has moved
        // since the last mousemove
        for (var i = 0; i < joueurs.length; i++) {
            var r = joueurs[i];
            if (r.isDragging) {
                r.x += dx;
                r.y += dy;
                if(r.sedeplace == true){
                 var  a = deple.length/4;
                 for(var h = 0;h<a;h++){
                    if(deple[h*4].jbase == i){
                       deple[h*4].x += dx;
                       deple[h*4].y += dy;
                   }
               }
               var b = depdeu.length/2;
               for(var h = 0;h<b;h++){
                if(depdeu[0+h*2].jbase == i){
                    depdeu[0+h*2].x += dx;
                    depdeu[0+h*2].y += dy;
                }
            }      
        }
        if(r.dribbb == true){
         var  a = dribblement.length/4;
         for(var h = 0;h<a;h++){
            if(dribblement[h*4].dbase == i){
               dribblement[h*4].x += dx;
               dribblement[h*4].y += dy;
           }
       }
       var b = drideu.length/2;
       for(var h = 0;h<b;h++){
        if(drideu[0+h*2].dbase == i){
            drideu[0+h*2].x += dx;
            drideu[0+h*2].y += dy;
        }
    }      
}
}
}
for (var i = 0; i < adversaire.length; i++) {
    var r = adversaire[i];
    if (r.isDragging) {
        r.x += dx;
        r.y += dy;
        r.a += dx;
        r.b += dy;
        r.c += dy;
        r.d += dx;
    }
}

for (var i = 0; i < zone.length; i++) {
    var r = zone[i];
    if (r.isDragging) {
        r.x += dx;
        r.y += dy;
    }
}
for (var i = 0; i < deple.length; i++) {
    var r = deple[i];
    if (r.isDragging) {
        if(i%4 != 0){
          r.x += dx;
          r.y += dy;
      }
  }
}

for (var i = 0; i < depdeu.length; i++) {
    var r = depdeu[i];
    if (r.isDragging) {
        if(i%2 != 0){
          r.x += dx;
          r.y += dy;
      }
  }
}

for (var i = 0; i < dribblement.length; i++) {
    var r = dribblement[i];
    if (r.isDragging) {
        if(i%4 != 0){
          r.x += dx;
          r.y += dy;
      }
  }
}

for (var i = 0; i < drideu.length; i++) {
    var r = drideu[i];
    if (r.isDragging) {
        if(i%2 != 0){
          r.x += dx;
          r.y += dy;
      }
  }
}


        // redraw the scene with the new rect positions
        draw();

        // reset the starting mouse position for the next mousemove
        startX = mx;
        startY = my;

    }
}


function ajouterjoueur() {
    var c = joueurs.length;
    var ch = [];
    var manqu = [];
    for (var d =0; d<c;d++){
        var f = joueurs[d];
        if(f.nom == 1){
            ch.push(1);
        }
        if(f.nom == 2){
            ch.push(2);
        }
        if(f.nom == 3){
            ch.push(3);
        }
        if(f.nom == 4){
            ch.push(4);
        }
        if(f.nom == 5){
            ch.push(5);
        }
    }
    if(ch.includes(1) == false){
        manqu.push(1);
    }
    if(ch.includes(2) == false){
        manqu.push(2);
    }
    if(ch.includes(3) == false){
        manqu.push(3);
    }
    if(ch.includes(4) == false){
        manqu.push(4);
    }
    if(ch.includes(5) == false){
        manqu.push(5);
    }
    console.log(manqu);
    var nom = '';
    if(joueurs.length == 0){
        nom = nom.concat(c+1);
    }
    else{
        nom = nom.concat(manqu[0]);
        manqu.splice(0,1);
    }

    if (joueurs.length >= 5)
        window.alert("Nombre de joueurs maximum atteint");
    else {
        joueurs.push({
            x: 60,
            y: 35 + ((joueurs.length + 1) * 50),
            rayon: taillejoueur,
            cache: 0,
            taille: Math.PI*2,
            last: false,
            nom: nom,
            fill: "red",
            isDragging: false,
            setball : false,
            gottheball : false,
            sedeplace : false,
            tiring : false,
        });
        last.push({
            action : 1,
        });
        if (document.getElementById("resultat").value == null ) {
            cercle(60,35 + (joueurs.length * 50),taillejoueur,0,Math.PI*2,false,"red",nom);        
        }
        else
            cercle(60,35 + (joueurs.length * 50),taillejoueur,0,Math.PI*2,false,document.getElementById("resultat").value,nom);

    }
    console.log(joueurs);
}


function ajouteradversaire() {

    if (adversaire.length >= 5)
        window.alert("Nombre d'adversaire maximum atteint");
    else {
        adversaire.push({
            x: 560,
            y: 35 + ((adversaire.length + 1) * 50),
            rayon: tailledefenseur,
            cache: 0,
            taille: Math.PI*2,
            last: false,
            fill: "black",
            isDragging: false,
            setball : false,
            hig : false,
            a : 560,
            b : 35 + (((adversaire.length + 1)* 50)),
            angle : Math.PI/2,
            inverse : -Math.PI/2,
            rretour : 13,
            rangle : 2 * Math.PI,
        });
        last.push({
            action : 2,
        });
        //cercle(560,35 + (adversaire.length * 50),tailledefenseur,0,Math.PI*2,false,"white");
        ligneappui(560,35 + (adversaire.length * 50)+tailledefenseur,560-13,35 + (adversaire.length * 50)+tailledefenseur);
        ligneappui(560,35 + (adversaire.length * 50)-tailledefenseur,560-13,35 + (adversaire.length * 50)-tailledefenseur);
        tx = 560 + tailledefenseur*Math.cos(Math.PI/2);
        ty = 35 + (adversaire.length * 50)+ tailledefenseur *Math.sin(Math.PI/2);
        px = 560 + tailledefenseur*Math.cos((-Math.PI/2));
        py = 35 + (adversaire.length * 50)+ tailledefenseur *Math.sin((-Math.PI/2));
        ligneappui(px,py,tx,ty,false);
        
    }
}

function fairepasse(a,b) {

    passes.push({
        x: joueurs[a.c],
        y: joueurs[a.c],
        x2 : joueurs[b.c],
        y2 : joueurs[b.c],
    });

    jpass.pop();
    jpass.pop();
    last.push({
        action : 5,
        depart : a.c,
        arrivé : b.c,
    });
    lignePasse(joueurs[a.c].x,joueurs[a.c].y,joueurs[b.c].x,joueurs[b.c].y);
    ;
}
function faireDemiTerrainHaut() {
    if(type_terrain != 2 && data() == true) {
        let c = confirm("Attention ! En changeant de terrain, les éléments actuels seront supprimés.");
        if (c == false) {
            return;
        }
        clearData();
    }

    clear();
    

    type_terrain = 2;
    ctx.lineWidth="5"; 
    
    //Bordures du terrain
    ctx.beginPath();
    ctx.setLineDash([]);
    ctx.strokeStyle =document.getElementById("resultatdelaligne").value;
    ctx.rect(l-lt*0.05, 0, long+lt*0.1, ht-ht*0.075);
    ctx.fillStyle=document.getElementById("resultatdelatouche").value;
    ctx.fill();
    ctx.stroke();

    //Lignes de touche
    ctx.beginPath();
    ctx.strokeStyle =document.getElementById("resultatdelaligne").value;
    ctx.setLineDash([]);
    ctx.fillStyle=document.getElementById("resultatter").value;
    ctx.rect(l, 0.075*ht, long, long*0.93);
    ctx.fill();
    ctx.stroke();

    haut = long*0.93;



    ctx.lineWidth="2"; 

    //Cercle central
    ctx.beginPath();
    ctx.setLineDash([]);
    ctx.strokeStyle =document.getElementById("resultatdelaligne").value;
    ctx.arc(l+long/2, ht-ht*0.145, lt*0.052, Math.PI, 0, false);
    ctx.fillStyle= document.getElementById("resultatinter").value;
    ctx.fill();
    ctx.stroke();


    /* RAQUETTES */
    //Rectangle raquette gauche
    ctx.beginPath();
    ctx.setLineDash([]);
    ctx.strokeStyle =document.getElementById("resultatdelaligne").value;
    ctx.rect(l+long/3, ht*0.075, long/3, long*0.405);
    ctx.fillStyle= document.getElementById("resultatinter").value;
    ctx.fill();
    ctx.stroke();

    //Cercle raquette gauche
    ctx.beginPath();
    ctx.setLineDash([]);
    ctx.strokeStyle =document.getElementById("resultatdelaligne").value;
    ctx.arc(l+long/2, long*0.495, lt*0.064, 0, Math.PI, false);
    ctx.fillStyle= document.getElementById("resultatinter").value;
    ctx.fill();
    ctx.stroke();

    /* LIGNES LANCERS FRANCS */
    //Ligne lf 1 gauche
    ctx.beginPath();
    ctx.setLineDash([]);
    ctx.strokeStyle =document.getElementById("resultatdelaligne").value;
    ctx.moveTo(l+0.06*long, ht*0.075);
    ctx.lineTo(l+0.06*long, 0.3*long);
    ctx.stroke();

    //Ligne lf 2 gauche
    ctx.beginPath();
    ctx.setLineDash([]);
    ctx.strokeStyle =document.getElementById("resultatdelaligne").value;
    ctx.moveTo(l+0.94*long, ht*0.075);
    ctx.lineTo(l+0.94*long, 0.3*long);
    ctx.stroke();

    // Cercle lf gauche
    ctx.beginPath();
    ctx.setLineDash([]);
    ctx.strokeStyle =document.getElementById("resultatdelaligne").value;
    ctx.arc(l+long/2, 0.197*long+0.075*ht, long*0.88/2, 0, Math.PI, false);
    ctx.stroke();


    ctx.stroke();


    /* PANIERS */
    //Cercle panier gauche
    ctx.beginPath();
    ctx.setLineDash([]);
    ctx.strokeStyle =document.getElementById("resultatdelaligne").value;
    ctx.arc(l+long/2, lt*0.098-ht*0.075, ht*0.083, 0, Math.PI, false);
    ctx.fillStyle=document.getElementById("resultatinter").value;
    ctx.fill();
    ctx.stroke();

    //Panier gauche
    ctx.beginPath();
    ctx.setLineDash([]);
    ctx.strokeStyle =document.getElementById("resultatdelaligne").value;
    ctx.moveTo(l+0.42*long, lt*0.098-ht*0.075);
    ctx.lineTo(l+0.58*long, lt*0.098-ht*0.075);
    ctx.fillStyle=document.getElementById("resultatinter").value;
    ctx.fill();
    ctx.stroke();

    ctx.beginPath();
    ctx.setLineDash([]);
    ctx.strokeStyle =document.getElementById("resultatdelaligne").value;
    ctx.arc((l+0.44*long)+(0.12*long/2), lt*0.07, ht*0.01, 0, Math.PI*2, false);
    ctx.stroke();

    ctx.beginPath();
    ctx.setLineDash([]);
    ctx.strokeStyle =document.getElementById("resultatdelaligne").value;
    ctx.moveTo((l+0.44*long)+(0.12*long/2), lt*0.098-ht*0.075);
    ctx.lineTo((l+0.44*long)+(0.12*long/2), lt*0.065);
    ctx.stroke();

    ctx.lineWidth="2"; 

}

function faireDemiTerrainBas() {
    if(type_terrain != 1 && data() == true) {
        let c = confirm("Attention ! En changeant de terrain, les éléments actuels seront supprimés.");
        if (c == false) {
            return;
        }
        clearData();
    }

    clear();

    type_terrain = 1;
    ctx.lineWidth="5"; 
    
    //Bordures du terrain
    ctx.beginPath();
    ctx.setLineDash([]);
    ctx.strokeStyle =document.getElementById("resultatdelaligne").value;
    ctx.rect(l-lt*0.05, 0, long+lt*0.1, ht-ht*0.075);
    ctx.fillStyle=document.getElementById("resultatdelatouche").value;
    ctx.fill();
    ctx.stroke();

    //Lignes de touche
    ctx.beginPath();
    ctx.strokeStyle =document.getElementById("resultatdelaligne").value;
    ctx.setLineDash([]);
    ctx.fillStyle=document.getElementById("resultatter").value;
    
    ctx.rect(l, 0.075*ht, long, long*0.93);
    ctx.fill();
    ctx.stroke();

    haut = long*0.93;



    ctx.lineWidth="2"; 

    //Cercle central
    ctx.beginPath();
    ctx.setLineDash([]);
    ctx.strokeStyle =document.getElementById("resultatdelaligne").value;
    ctx.arc(l+long/2, 0.075*ht, lt*0.052, 0, Math.PI, false);
    ctx.fillStyle= document.getElementById("resultatinter").value;
    ctx.fill();
    ctx.stroke();


    /* RAQUETTES */
    //Rectangle raquette gauche
    ctx.beginPath();
    ctx.setLineDash([]);
    ctx.strokeStyle =document.getElementById("resultatdelaligne").value;
    ctx.rect(l+long/3, long-long*0.388, long/3, long*0.405);
    ctx.fillStyle= document.getElementById("resultatinter").value;
    ctx.fill();
    ctx.stroke();

    //Cercle raquette gauche
    ctx.beginPath();
    ctx.setLineDash([]);
    ctx.strokeStyle =document.getElementById("resultatdelaligne").value;
    ctx.arc(l+long/2, long-long*0.388, lt*0.064, Math.PI, 0, false);
    ctx.fillStyle= document.getElementById("resultatinter").value;
    ctx.fill();
    ctx.stroke();

    /* LIGNES LANCERS FRANCS */
    //Ligne lf 1 gauche
    ctx.beginPath();
    ctx.setLineDash([]);
    ctx.strokeStyle =document.getElementById("resultatdelaligne").value;
    ctx.moveTo(l+0.06*long, haut+ht*0.075);
    ctx.lineTo(l+0.06*long, 0.803*long);
    ctx.stroke();

    //Ligne lf 2 gauche
    ctx.beginPath();
    ctx.setLineDash([]);
    ctx.strokeStyle =document.getElementById("resultatdelaligne").value;
    ctx.moveTo(l+0.94*long, haut+ht*0.075);
    ctx.lineTo(l+0.94*long, 0.803*long);
    ctx.stroke();

    // Cercle lf gauche
    ctx.beginPath();
    ctx.setLineDash([]);
    ctx.strokeStyle =document.getElementById("resultatdelaligne").value;
    ctx.arc(l+long/2, 0.803*long, long*0.88/2, Math.PI, 0, false);
    ctx.stroke();


    ctx.stroke();


    /* PANIERS */
    //Cercle panier gauche
    ctx.beginPath();
    ctx.setLineDash([]);
    ctx.strokeStyle =document.getElementById("resultatdelaligne").value;
    ctx.arc(l+long/2, long*0.922, ht*0.083, Math.PI, 0, false);
    ctx.fillStyle=document.getElementById("resultatinter").value;
    ctx.fill();
    ctx.stroke();

    //Panier gauche
    ctx.beginPath();
    ctx.setLineDash([]);
    ctx.strokeStyle =document.getElementById("resultatdelaligne").value;
    ctx.moveTo(l+0.42*long, haut);
    ctx.lineTo(l+0.58*long, haut);
    ctx.fillStyle=document.getElementById("resultatinter").value;
    ctx.fill();
    ctx.stroke();

    ctx.beginPath();
    ctx.setLineDash([]);
    ctx.strokeStyle =document.getElementById("resultatdelaligne").value;
    ctx.arc((l+0.44*long)+(0.12*long/2), haut*0.975, ht*0.01, 0, Math.PI*2, false);
    ctx.stroke();

    ctx.beginPath();
    ctx.setLineDash([]);
    ctx.strokeStyle =document.getElementById("resultatdelaligne").value;
    ctx.moveTo((l+0.44*long)+(0.12*long/2), haut);
    ctx.lineTo((l+0.44*long)+(0.12*long/2), haut*0.986);
    ctx.stroke();

    ctx.lineWidth="2"; 

}

function faireterrain() {

    ctx.lineWidth=tailleligneP; 

//Bordures du terrain





ctx.beginPath();
if(type_terrain != 0 && data() == true) {
    let c = confirm("Attention ! En changeant de terrain, les éléments actuels seront supprimés.");
    if (c == false) {
        return;
    }
    clearData();
}

type_terrain = 0;
ctx.setLineDash([]);
ctx.strokeStyle =document.getElementById("resultatdelaligne").value;
ctx.rect(0, 0, lt, ht);
ctx.fillStyle=document.getElementById("resultatdelatouche").value;
ctx.fill();
ctx.stroke();

ctx.beginPath();
ctx.strokeStyle=document.getElementById("resultatdelaligne").value;
ctx.setLineDash([]);
ctx.rect(0.05*lt, 0.075*ht, 0.9*lt, 0.85*ht);
ctx.fillStyle=document.getElementById("resultatter").value;
ctx.fill();

ctx.stroke();



ctx.lineWidth=tailleligneN; 

//Cercle central
ctx.beginPath();
ctx.setLineDash([]);
ctx.strokeStyle =document.getElementById("resultatdelaligne").value;
ctx.arc(lt/2, ht/2, lt*0.052, 0, Math.PI*2, false);
ctx.fillStyle= document.getElementById("resultatinter").value;
ctx.fill();
ctx.stroke();

//Ligne centrale
ctx.beginPath();
ctx.setLineDash([]);
ctx.strokeStyle =document.getElementById("resultatdelaligne").value;
ctx.moveTo(lt/2, ht*0.925);
ctx.lineTo(lt/2, ht*0.075);
ctx.stroke();

/* RAQUETTES */
//Rectangle raquette gauche
ctx.beginPath();
ctx.setLineDash([]);
ctx.strokeStyle =document.getElementById("resultatdelaligne").value;
ctx.rect(0.052*lt, ht/3, lt*0.208, ht/3);
ctx.fillStyle= document.getElementById("resultatinter").value;
ctx.fill();
ctx.stroke();

//Rectangle raquette droite
ctx.beginPath();  
ctx.setLineDash([]);
ctx.strokeStyle =document.getElementById("resultatdelaligne").value;
ctx.rect(lt-lt*0.260, ht/3, lt-lt*0.792, ht/3);
ctx.fillStyle= document.getElementById("resultatinter").value;
ctx.fill();
ctx.stroke();

//Cercle raquette gauche
ctx.beginPath();
ctx.setLineDash([]);
ctx.strokeStyle =document.getElementById("resultatdelaligne").value;
ctx.arc(lt*0.260, ht/2, lt*0.064, 1.5*Math.PI, 0.5*Math.PI, false);
ctx.fillStyle= document.getElementById("resultatinter").value;
ctx.fill();
ctx.stroke();

//Cercle raquette droite
ctx.beginPath();
ctx.setLineDash([]);
ctx.strokeStyle =document.getElementById("resultatdelaligne").value;
ctx.arc(lt-(lt*0.260), ht/2, lt*0.064, 0.5*Math.PI, 1.5*Math.PI, false);
ctx.fillStyle= document.getElementById("resultatinter").value;
ctx.fill();
ctx.stroke();


/* LIGNES LANCERS FRANCS */
//Ligne lf 1 gauche
ctx.beginPath();
ctx.setLineDash([]);
ctx.strokeStyle =document.getElementById("resultatdelaligne").value;
ctx.moveTo(0.050*lt, 0.12*ht);
ctx.lineTo(lt*0.175, 0.12*ht);
ctx.stroke();

//Ligne lf 2 gauche
ctx.beginPath();
ctx.setLineDash([]);
ctx.strokeStyle =document.getElementById("resultatdelaligne").value;
ctx.moveTo(0.050*lt, 0.88*ht);
ctx.lineTo(lt*0.175, 0.88*ht);
ctx.stroke();

// Cercle lf gauche
ctx.beginPath();
ctx.setLineDash([]);
ctx.strokeStyle =document.getElementById("resultatdelaligne").value;
ctx.arc(lt*0.17, ht/2, ht*0.38, 1.5*Math.PI, 0.5*Math.PI, false);
ctx.stroke();


//Ligne lf 1 droite
ctx.beginPath();
ctx.setLineDash([]);
ctx.strokeStyle =document.getElementById("resultatdelaligne").value;
ctx.moveTo(lt*0.950, 0.12*ht);
ctx.lineTo(lt*0.825, 0.12*ht);
ctx.stroke();

//Ligne lf 2 droite
ctx.beginPath();
ctx.setLineDash([]);
ctx.strokeStyle =document.getElementById("resultatdelaligne").value;
ctx.moveTo(lt*0.950, 0.88*ht);
ctx.lineTo(lt*0.825, 0.88*ht);
ctx.stroke();

//Cercle lf droite
ctx.beginPath();
ctx.setLineDash([]);
ctx.strokeStyle =document.getElementById("resultatdelaligne").value;
ctx.arc(lt*0.825, ht/2, lt*0.203, 0.5*Math.PI, 1.5*Math.PI, false);

ctx.stroke();


/* PANIERS */
//Cercle panier gauche
ctx.beginPath();
ctx.setLineDash([]);
ctx.strokeStyle =document.getElementById("resultatdelaligne").value;
ctx.arc(lt*0.078, ht/2, ht*0.083, 1.5*Math.PI, 0.5*Math.PI, false);
ctx.fillStyle=document.getElementById("resultatinter").value;
ctx.fill();
ctx.stroke();

//Panier gauche
ctx.beginPath();
ctx.setLineDash([]);
ctx.strokeStyle =document.getElementById("resultatdelaligne").value;
ctx.moveTo(lt*0.08, ht*0.44);
ctx.lineTo(lt*0.08, ht*0.56);
ctx.fillStyle=document.getElementById("resultatinter").value;
ctx.fill();
ctx.stroke();

ctx.beginPath();
ctx.setLineDash([]);
ctx.strokeStyle =document.getElementById("resultatdelaligne").value;
ctx.arc(lt*0.092, ht/2, ht*0.01, 0, Math.PI*2, false);
ctx.stroke();

ctx.beginPath();
ctx.setLineDash([]);
ctx.strokeStyle =document.getElementById("resultatdelaligne").value;
ctx.moveTo(lt*0.08, ht/2);
ctx.lineTo(lt*0.087, ht/2);
ctx.stroke();


//Cercle panier droit
ctx.beginPath();
ctx.setLineDash([]);
ctx.strokeStyle =document.getElementById("resultatdelaligne").value;
ctx.arc(lt-lt*0.078, ht/2, ht*0.083, 0.5*Math.PI, 1.5*Math.PI, false);
ctx.stroke();

//Panier droit
ctx.beginPath();
ctx.setLineDash([]);
ctx.strokeStyle =document.getElementById("resultatdelaligne").value;
ctx.moveTo(lt-lt*0.08, ht-ht*0.44);
ctx.lineTo(lt-lt*0.08, ht-ht*0.56);
ctx.stroke();


ctx.beginPath();
ctx.setLineDash([]);
ctx.strokeStyle =document.getElementById("resultatdelaligne").value;
ctx.moveTo(lt-lt*0.08, ht-ht/2);
ctx.lineTo(lt-lt*0.087, ht-ht/2);
ctx.stroke();

ctx.beginPath();
ctx.setLineDash([]);
ctx.strokeStyle =document.getElementById("resultatdelaligne").value;
ctx.arc(lt-lt*0.092, ht/2, ht*0.01, 0, Math.PI*2, false);
ctx.stroke();


ctx.beginPath();
ctx.setLineDash([]);
ctx.stroke();
}

function zoner(){
    if(edit == true){
        le_contexte.beginPath();
        le_contexte.fillStyle = "rgb("+r5+","+g5+","+b5+",0.3)";
        le_contexte.strokeStyle = "rgb(255,0,0)";
        cercleedit(zone[0].x,zone[0].y,15,0,Math.PI*2,"rgba(128,128,128,0.3)");
        for (j = 1; j< nbpoints;j++){
            cercleedit(zone[j].x,zone[j].y,15,0,Math.PI*2,"rgba(128,128,128,0.3)");
        }
        le_contexte.moveTo(zone[0].x, zone[0].y);
        for (i = 1; i< nbpoints;i++){
            le_contexte.lineTo(zone[i].x, zone[i].y);
        }
        le_contexte.lineTo(zone[0].x, zone[0].y);
        le_contexte.fill();
        le_contexte.stroke();


    }
    else{
        le_contexte.beginPath();
        le_contexte.fillStyle = "rgb("+r5+","+g5+","+b5+",0.3)";
        le_contexte.strokeStyle = "rgb(255,0,0)";
        le_contexte.moveTo(zone[0].x, zone[0].y);
        for (i = 1; i< nbpoints;i++)
            le_contexte.lineTo(zone[i].x, zone[i].y);
        le_contexte.lineTo(zone[0].x, zone[0].y);
        le_contexte.fill();
        le_contexte.stroke();
    }

}


function deplacer(){
    if ( edit == true){
        for(i = 0;i< (nbptsdep/4);i++){
            for(j=1;j<3;j++){
                cercleedit(deple[j+i*4].x,deple[j+i*4].y,15,0,Math.PI*2,"rgba(128,128,128,0.9)");
            }
            ctx.beginPath();
            ctx.strokeStyle = "black";
            for(h = 0;h< (nbptsdep/4);h++){

                ctx.moveTo(deple[0+h*4].x,deple[0+h*4].y);
                ctx.bezierCurveTo(deple[1+h*4].x,deple[1+h*4].y,deple[2+h*4].x,deple[2+h*4].y,deple[3+h*4].x,deple[3+h*4].y);
                if(deple[3+h*4].ec == false){
                    ctx.stroke();
                    ctx.closePath();
                    canvas_arrow2(le_contexte, deple[2+h*4].x, deple[2+h*4].y, deple[3+h*4].x, deple[3+h*4].y);

                }
                else{
                    ctx.moveTo(deple[0+h*4].x,deple[0+h*4].y);
                    ctx.bezierCurveTo(deple[1+h*4].x,deple[1+h*4].y,deple[2+h*4].x,deple[2+h*4].y,deple[3+h*4].x,deple[3+h*4].y);
                    a = deple[3+h*4].x + deple[3+h*4].rayon*Math.cos(deple[3+h*4].angle);
                    b = deple[3+h*4].y + deple[3+h*4].rayon*Math.sin(deple[3+h*4].angle);
                    c = deple[3+h*4].x + deple[3+h*4].rayon*Math.cos(deple[3+h*4].inverse);
                    d = deple[3+h*4].y + deple[3+h*4].rayon*Math.sin(deple[3+h*4].inverse);
                    
                    if(deple[3+h*4].hig == true){
                        ctx.lineWidth = 5;
                    }
                    else{
                        ctx.lineWidth = 2;
                    }
                    ctx.setLineDash([]);
                    ctx.strokeStyle ="black";
                    ctx.moveTo(a, b);
                    ctx.lineTo(c, d);
                    ctx.stroke();
                }
            }
        }
        le_contexte.stroke();
    }
    else{
        if(deple.length > 0 && deple.length %4 == 0){
            ctx.beginPath();
            ctx.strokeStyle = "black";
            for(i = 0;i< (nbptsdep/4);i++){
                if(deple[3+i*4].ec == false){
                    ctx.moveTo(deple[0+i*4].x,deple[0+i*4].y);
                    ctx.bezierCurveTo(deple[1+i*4].x,deple[1+i*4].y,deple[2+i*4].x,deple[2+i*4].y,deple[3+i*4].x,deple[3+i*4].y);
                    ctx.stroke();
                    ctx.closePath();
                    canvas_arrow2(le_contexte, deple[2+i*4].x, deple[2+i*4].y, deple[3+i*4].x, deple[3+i*4].y);
                }
                else{
                    le_contexte.moveTo(deple[0+i*4].x,deple[0+i*4].y);
                    le_contexte.bezierCurveTo(deple[1+i*4].x,deple[1+i*4].y,deple[2+i*4].x,deple[2+i*4].y,deple[3+i*4].x,deple[3+i*4].y);
                    a = deple[3+i*4].x + deple[3+i*4].rayon*Math.cos(deple[3+i*4].angle);
                    b = deple[3+i*4].y + deple[3+i*4].rayon*Math.sin(deple[3+i*4].angle);
                    c = deple[3+i*4].x + deple[3+i*4].rayon*Math.cos(deple[3+i*4].inverse);
                    d = deple[3+i*4].y + deple[3+i*4].rayon*Math.sin(deple[3+i*4].inverse);
                    if(deple[3+i*4].hig == true){
                        le_contexte.lineWidth = 5;
                    }
                    else{
                        le_contexte.lineWidth = 2;
                    }
                    le_contexte.setLineDash([]);
                    le_contexte.strokeStyle ="black";
                    le_contexte.moveTo(a, b);
                    le_contexte.lineTo(c, d);
                    le_contexte.stroke();
                }

            }
            ctx.stroke();
            le_contexte.stroke();
        }
    }
}

function depladeu(){
    le_contexte.beginPath();
    le_contexte.strokeStyle = "black";
    if(edit == true){	
       for(i = 0; i< (nbdepdeu/2);i++){
        cercleedit(depdeu[1+i*2].x,depdeu[1+i*2].y,15,0,Math.PI*2,"rgba(128,128,128,0.9)");
        if(depdeu[1+i*2].ec == false){
            le_contexte.moveTo(depdeu[0+i*2].x,depdeu[0+i*2].y);
            le_contexte.lineTo(depdeu[1+i*2].x,depdeu[1+i*2].y);
            canvas_arrow2(le_contexte, depdeu[0+i*2].x, depdeu[0+i*2].y, depdeu[1+i*2].x, depdeu[1+i*2].y);
        }
        else{
            le_contexte.moveTo(depdeu[0+i*2].x,depdeu[0+i*2].y);
            le_contexte.lineTo(depdeu[1+i*2].x,depdeu[1+i*2].y);
            a = depdeu[1+i*2].x + depdeu[1+i*2].rayon*Math.cos(depdeu[1+i*2].angle);
            b = depdeu[1+i*2].y + depdeu[1+i*2].rayon*Math.sin(depdeu[1+i*2].angle);
            c = depdeu[1+i*2].x + depdeu[1+i*2].rayon*Math.cos(depdeu[1+i*2].inverse);
            d = depdeu[1+i*2].y + depdeu[1+i*2].rayon*Math.sin(depdeu[1+i*2].inverse);
            if(depdeu[1+i*2].hig == true){
                le_contexte.lineWidth = 5;
            }
            else{
                le_contexte.lineWidth = 2;
            }
            le_contexte.setLineDash([]);
            le_contexte.strokeStyle ="black";
            le_contexte.moveTo(a, b);
            le_contexte.lineTo(c, d);
            le_contexte.stroke();
        }
    }
    le_contexte.stroke();
}
else{     
    ctx.beginPath();
    ctx.strokeStyle = "black";    
    for(i = 0; i< (nbdepdeu/2);i++){
        if(depdeu[1+i*2].ec == false){
            ctx.moveTo(depdeu[0+i*2].x,depdeu[0+i*2].y);
            ctx.lineTo(depdeu[1+i*2].x,depdeu[1+i*2].y);
            ctx.stroke();
            ctx.closePath();
            canvas_arrow2(le_contexte, depdeu[0+i*2].x, depdeu[0+i*2].y, depdeu[1+i*2].x, depdeu[1+i*2].y);
        }
        else{
            ctx.moveTo(depdeu[0+i*2].x,depdeu[0+i*2].y);
            ctx.lineTo(depdeu[1+i*2].x,depdeu[1+i*2].y);
            a = depdeu[1+i*2].x + depdeu[1+i*2].rayon*Math.cos(depdeu[1+i*2].angle);
            b = depdeu[1+i*2].y + depdeu[1+i*2].rayon*Math.sin(depdeu[1+i*2].angle);
            c = depdeu[1+i*2].x + depdeu[1+i*2].rayon*Math.cos(depdeu[1+i*2].inverse);
            d = depdeu[1+i*2].y + depdeu[1+i*2].rayon*Math.sin(depdeu[1+i*2].inverse);
            if(depdeu[1+i*2].hig == true){
                ctx.lineWidth = 5;
            }
            else{
                ctx.lineWidth = 2;
            }
            ctx.setLineDash([]);
            ctx.moveTo(a, b);
            ctx.lineTo(c, d);
            ctx.stroke();
            ctx.closePath();
        }
    }

}
}

function dribdeu(){
    le_contexte.beginPath();
    le_contexte.strokeStyle = "black";
    if(edit == true){      
        for(i = 0; i< (nbdrideu/2);i++){
            cercleedit(drideu[1+i*2].x,drideu[1+i*2].y,15,0,Math.PI*2,"rgba(128,128,128,0.9)");
            ligneDrible(drideu[0+i*2].x, drideu[0+i*2].y, drideu[1+i*2].x, drideu[1+i*2].y);
            canvas_arrow2(le_contexte, drideu[0+i*2].x, drideu[0+i*2].y, drideu[1+i*2].x, drideu[1+i*2].y);

        }
        le_contexte.stroke();
    }
    else{ 
        for(i = 0; i< (nbdrideu/2);i++){
            ligneDrible(drideu[0+i*2].x, drideu[0+i*2].y, drideu[1+i*2].x, drideu[1+i*2].y);
            canvas_arrow2(le_contexte, drideu[0+i*2].x, drideu[0+i*2].y, drideu[1+i*2].x, drideu[1+i*2].y);

        }
        le_contexte.stroke();

    }
}

function dribblage(){
    if ( edit == true){
        ctx.beginPath();
        ctx.strokeStyle = "black";
        for(k = 0;k< (nbptsdib/4);k++){
            for(j=1;j<3;j++){
                cercleedit(dribblement[j+k*4].x,dribblement[j+k*4].y,15,0,Math.PI*2,"rgba(128,128,128,0.9)");
            }
            for(i = 0;i< (nbptsdib/4);i++){
                ctx.moveTo(dribblement[0+i*4].x,dribblement[0+i*4].y);
                var coord = getBezierXY(0.05,dribblement[0+i*4].x, dribblement[0+i*4].y, dribblement[1+i*4].x,dribblement[1+i*4].y, dribblement[2+i*4].x, dribblement[2+i*4].y, dribblement[3+i*4].x, dribblement[3+i*4].y);
                centreIntervalle(dribblement[0+i*4].x,dribblement[0+i*4].y,coord.x,coord.y,nbIntervalle,intervalleDroite(dribblement[0+i*4].x,dribblement[0+i*4].y,coord.x,coord.y),true);
                for(var fd = 0.05; fd < 0.95; fd = fd +0.05){
                    var coord = getBezierXY(fd,dribblement[0+i*4].x, dribblement[0+i*4].y, dribblement[1+i*4].x,dribblement[1+i*4].y, dribblement[2+i*4].x, dribblement[2+i*4].y, dribblement[3+i*4].x, dribblement[3+i*4].y);
                    var coord1 = getBezierXY(fd+0.05,dribblement[0+i*4].x, dribblement[0+i*4].y, dribblement[1+i*4].x,dribblement[1+i*4].y, dribblement[2+i*4].x, dribblement[2+i*4].y, dribblement[3+i*4].x, dribblement[3+i*4].y);
                    centreIntervalle(coord.x,coord.y,coord1.x,coord1.y,nbIntervalle,intervalleDroite(coord.x,coord.y,coord1.x,coord1.y),true);
                }
                var coord19 = getBezierXY(0.98,dribblement[0+i*4].x, dribblement[0+i*4].y, dribblement[1+i*4].x,dribblement[1+i*4].y, dribblement[2+i*4].x, dribblement[2+i*4].y, dribblement[3+i*4].x, dribblement[3+i*4].y);
                ctx.lineTo(coord19.x,coord19.y);
                ctx.stroke();
                ctx.closePath();
                canvas_arrow2(le_contexte, dribblement[2+i*4].x, dribblement[2+i*4].y, dribblement[3+i*4].x, dribblement[3+i*4].y);
            }
        }
    }
    else{
        if(dribblement.length > 0 && dribblement.length %4 == 0){
            ctx.beginPath();
            ctx.strokeStyle = "black";
            for(i = 0;i< (nbptsdib/4);i++){
                ctx.moveTo(dribblement[0+i*4].x,dribblement[0+i*4].y);
                var coord = getBezierXY(0.05,dribblement[0+i*4].x, dribblement[0+i*4].y, dribblement[1+i*4].x,dribblement[1+i*4].y, dribblement[2+i*4].x, dribblement[2+i*4].y, dribblement[3+i*4].x, dribblement[3+i*4].y);
                centreIntervalle(dribblement[0+i*4].x,dribblement[0+i*4].y,coord.x,coord.y,nbIntervalle,intervalleDroite(dribblement[0+i*4].x,dribblement[0+i*4].y,coord.x,coord.y),true);
                for(var fd = 0.05; fd < 0.95; fd = fd +0.05){
                    var coord = getBezierXY(fd,dribblement[0+i*4].x, dribblement[0+i*4].y, dribblement[1+i*4].x,dribblement[1+i*4].y, dribblement[2+i*4].x, dribblement[2+i*4].y, dribblement[3+i*4].x, dribblement[3+i*4].y);
                    var coord1 = getBezierXY(fd+0.05,dribblement[0+i*4].x, dribblement[0+i*4].y, dribblement[1+i*4].x,dribblement[1+i*4].y, dribblement[2+i*4].x, dribblement[2+i*4].y, dribblement[3+i*4].x, dribblement[3+i*4].y);
                    centreIntervalle(coord.x,coord.y,coord1.x,coord1.y,nbIntervalle,intervalleDroite(coord.x,coord.y,coord1.x,coord1.y),true);
                }
                var coord19 = getBezierXY(1,dribblement[0+i*4].x, dribblement[0+i*4].y, dribblement[1+i*4].x,dribblement[1+i*4].y, dribblement[2+i*4].x, dribblement[2+i*4].y, dribblement[3+i*4].x, dribblement[3+i*4].y);
                ctx.lineTo(coord19.x,coord19.y);
                ctx.stroke();
                ctx.closePath();
                canvas_arrow2(le_contexte, dribblement[2+i*4].x, dribblement[2+i*4].y, dribblement[3+i*4].x, dribblement[3+i*4].y);

            }
        }
    }
}

function triggerdonnerlaballe(){
    if(ball == true){
        ball = false;
        untoggleDonner();
    }else{
        if(nbball >0){
            window.alert("On ne peut faire une passe si on a pas 2 joueurs");
        }
        if(passable || bouge || drib || ecra || tirable || zonable || edit || effacing){
            window.alert("Vous ne pouvez éffectuer cette action, une autre action est déja en cours");
        }
        else{
            toggleDonner();
            ball = true;
        }
    }
}

function triggerfairepasse(){
    if(joueurs.length < 2){
        window.alert("On ne peut faire une passe si on a pas 2 joueurs");
        return;
    }
    if(ball || bouge || drib || ecra || tirable || zonable || edit || effacing){
        window.alert("Vous ne pouvez éffectuer cette action, une autre action est déja en cours");
    }
    else{
        if(passable == true){
            passable = false;
            untogglePasse();
            return;
        }
        if(passable == false && joueurs.length >= 2){
            togglePasse();
            passable = true;
            return;

        }
    }
}

function addj(a){
   jpass.push({
    c : a,
});
}

function retourarriere(){

    x = last.length;
    if(x == 0){
        window.alert("Vous ne pouvez revenir en arriere")
    }
    if(ball || bouge || drib || ecra || tirable || zonable || edit || effacing || passable){
        window.alert("Vous ne pouvez éffectuer cette action, une autre action est déja en cours");
    }
    else{
        if(last[x-1].action == 1){
            joueurs.pop();
            last.pop();
            draw();
        }
        if(last[x-1].action == 2){
            adversaire.pop();
            last.pop();
            draw();
        }
        if(last[x-1].action == 3){

            for (var i = 0; i < joueurs.length; i++) {
                var r = joueurs[i];
                if(r.setball == true){
                    r.setball = false;
                    r.gottheball = false;
                    nbball = nbball - 1;
                    last.pop();
                    draw();
                }
            }
        }

        if(last[x-1].action == 4){

            for (var i = 0; i < adversaire.length; i++) {
                var r = adversaire[i];
                if(r.setball = true){
                    r.setball = false;
                    nbball = nbball - 1;
                    last.pop();
                    draw();
                }
            }
        }
        if(last[x-1].action == 5){
            joueurs[last[x-1].arrivé].gottheball = false;
            joueurs[last[x-1].depart].gottheball = true;
            last.pop();
            passes.pop();
            draw();

        }
        if(last[x-1].action == 6){
            while(zone.length != 0){
                zone.pop();
            }
            nbzone = 0;
            nbpoints = 0;
            last.pop();
            draw();

        }

        if(last[x-1].action == 7){
            for(i = 0; i< 4;i++){
                deple.pop();
            }
            nbptsdep = nbptsdep -4;
            last.pop();
            draw();

        }

        if(last[x-1].action == 8){
            for(i = 0; i< 4;i++){
                dribblement.pop();
            }
            nbptsdib = nbptsdib -4;
            last.pop();
            nbdrib = nbdrib -1;
            draw();

        }

        if(last[x-1].action == 9){
            for (var i = 0; i < joueurs.length; i++) {
                var r = joueurs[i];
                r.tiring = false;
            }
            nbtir = nbtir-1;
            last.pop();
            draw();

        }

        if(last[x-1].action == 10){
            for(i =0; i<2;i++){
                depdeu.pop();
            }
            nbdepdeu = nbdepdeu-2;
            last.pop();
            draw();

        }

        if(last[x-1].action == 11){
            for(i =0; i<2;i++){
                drideu.pop();
            }
            nbdrideu = nbdrideu-2;
            last.pop();
            draw();

        }

    }

}

function influer(){
    if(zonable == true){
        untogglezone();
        zonable = false;
        if(zonable == false && nbpoints > 2 && nbzone == 0){
            nbzone = 1;
            last.push({
                action : 6,
            });
            draw();
        }
    }
    else{
        if(ball || bouge || drib || ecra || tirable || edit || effacing){
           window.alert("Vous ne pouvez éffectuer cette action, une autre action est déja en cours");
       }
       else{
        zonable = true ;
        togglezone();
    }
}
}

function effacer(){
    if(effacing == true){
        effacing = false;
        untoggleEffacer();
    }
    else{
        if(ball || bouge || drib || ecra || tirable || zonable || edit){
            window.alert("Vous ne pouvez éffectuer cette action, une autre action est déja en cours");
        }
        else{
            effacing = true;
            toggleEffacer();
        }
    }
}


//Clear tous les éléments du terrains (joueurs, passes, adversaires...)
function clearData() {
    let length = [joueurs.length, adversaire.length, zone.length, passes.length, jpass.length, deple.length, depdeu.length, dribblement.length, drideu.length, last.length];
    for (let j = 0; j < length.length; j++){
        for (let i = 0; i < length[j]; i++) {
            joueurs.pop();
        }
        j++;
        for (let i = 0; i < length[j]; i++) {
            adversaire.pop();
        }
        j++;
        for (let i = 0; i < length[j]; i++) {
            zone.pop();
        }
        j++;
        for(let i = 0; i < length[j]; i++) {
            passes.pop();
        }
        j++;
        for(let i = 0; i < length[j]; i++) {
            jpass.pop();
        }
        j++;
        for(let i = 0; i < length[j]; i++) {
            deple.pop();
        }
        j++;
        for(let i = 0; i < length[j]; i++) {
            depdeu.pop();
        }
        j++;
        for(let i = 0; i < length[j]; i++) {
            dribblement.pop();
        }
        j++;
        for(let i = 0; i < length[j]; i++) {
            drideu.pop();
        }
        j++;
        for(let i = 0; i < length[j]; i++) {
            last.pop();
        }
        j++;
    }
    taillejoueur = 20;
    tailledefenseur = 20;
    nbIntervalle = 0;
    dragok = false;
    ball = false;
    nbball = 0;
    nba = 0;
    nbb = 0;
    tirable = false;
    nbtir = 0;
    nbzone = 0;
    zonable = false;
    nbpoints = 0;
    effacing = false;
    edit = false;
    ecra = false;
    bouge = false;
    nbptsdep = 0;
    currentdep = 0;
    nbbase = 0;
    nbdepdeu = 0;
    usabledep = false;
    currentdepun = false;
    currentdepdeu = false;
    currentdrib = false;
    nbdrideu = 0;
    nbptsdib = 0;
    drib = 0;
    nbbased = 0;
    nbdrib = 0;
    nombredepdeu = 0;
    nbjpass = 0;
    passable = false;
    nbclicpass = 0;
    dernierpass = null;
    nbhig = 0;
}

function data(){
    if (joueurs.length > 0 || adversaire.length > 0 || zone.length > 0) {
        return true;
    }
    return false;
}


function editer(){
    if(edit == false){
        if(ball || bouge || drib || ecra || tirable || zonable || effacing){
            window.alert("Vous ne pouvez éffectuer cette action, une autre action est déja en cours");
        }
        else{
            edit = true;
            draw();
            toggleediter();
        }
    }
    else{
        for (var i = 0; i < adversaire.length; i++) {
            adversaire[i].hig = false;
        }
        for (var i = 0;i<deple.length/4;i++){
            deple[3+i*4].hig = false;
        }
        for (var i = 0;i<depdeu.length/2;i++){
            depdeu[1+i*2].hig = false;
        }
        nbhig =0;
        edit = false;
        draw();
        untoggleediter();
        UntoggleOutilAppui();
    }
}


function deplace(){
    if(bouge == false){
        if(ball || drib || ecra || tirable || zonable || edit || effacing){
            window.alert("Vous ne pouvez éffectuer cette action, une autre action est déja en cours");
        }
        else{
            bouge = true;
            toggleDeplacer();
        //draw();
    }
}
else{
    bouge = false;
    untoggleDeplacer();
    if(nbptsdep >3 && currentdepun == true){
       for(j = 0;j<4;j++){
        depdeu.pop();
    }
    nbdepdeu = nbdepdeu -4;
    currentdep = 1;
    last.push({
        action : 7,
    });
    draw();
    usabledep = true;
    currentdepun = false;
}
if(nbdepdeu%2 == 0){
    while(deple.length%4 !=0){
        deple.pop();
    }
    while(nbptsdep%4 !=0){
        nbptsdep = nbptsdep-1;
    }
    nombredepdeu = nombredepdeu +1;
    last.push({
        action : 10,
    });
    draw();
    usabledep = true;

}
if(nbptsdep < 3 && nbdepdeu != 2 && !usabledep){
    window.alert("Il vous faut 4 points pour se deplacer en courbe ou 2 pour une ligne")
    while(deple.length%4 !=0){
        deple.pop();
    }
    nbdepdeu == 0;
    while(depdeu.length%4 !=0){
        depdeu.pop();
    }
    nbptsdep = 0;
    draw();
}
}
currentdep = false;
draw();
}


function dribble(){
    if(drib == false){
        if(ball || bouge || ecra || tirable || zonable || edit || effacing){
           window.alert("Vous ne pouvez éffectuer cette action, une autre action est déja en cours");
       }
       else{
        drib = true;
        toggleDribble();
        draw();
    }
}
else{
    drib = false;
    untoggleDribble();

    if(nbptsdib >3 && currentdrib){
        for(j = 0;j<4;j++){
            drideu.pop();
        }
        last.push({
            action : 8,
        });
        nbdrib = nbdrib +1;
        nbdrideu = nbdrideu -4;
        draw();
        currentdrib = false;

    }
    if(nbdrideu%2 == 0){
        console.log("ici");
        while(dribblement.length%4 !=0){
            dribblement.pop();
        }
        while(nbptsdib%4 !=0){
            nbptsdib = nbptsdib-1;
        }
        last.push({
            action : 11,
        });
        draw();
        usabledep = true;
        console.log(dribblement);
        console.log(nbptsdib);

    }
    if(nbptsdib < 3 && nbdrideu != 2 && !usabledep){
        window.alert("Il vous faut 4 points pour dribbler en courbe ou 2 pour une ligne")
        while(dribblement.length%4 !=0){
            dribblement.pop();
        }
        nbdrideu == 0;
        while(drideu.length%4 !=0){
            drideu.pop();
        }
        nbptsdib = 0;
        draw();
    }
}
}

function tire(){
    if(tirable == false){
        if(ball || bouge || drib || ecra || zonable || edit || effacing){
           window.alert("Vous ne pouvez éffectuer cette action, une autre action est déja en cours");
       }
       else{
        if(nbtir >0){
            window.alert("Un tir maximum par schéma");
            untoggletire();
        }
        else{
         tirable = true;
         toggletire();
     }     
 }
}
else{
    tirable = false;
    untoggletire();
}


}

function tournergauche(){
    for (var i = 0; i < adversaire.length; i++) {
        var r = adversaire[i];
        if(r.hig){
            r.angle = r.angle - Math.PI/10;
            r.inverse = r.inverse - Math.PI/10;
            r.rangle = r.rangle - Math.PI/10;
        }
    }
    for (var i = 0;i<deple.length/4;i++){
        var r = deple[3+i*4];
        if(r.hig){
            r.angle = r.angle - Math.PI/10;
            r.inverse = r.inverse - Math.PI/10;
            r.rangle = r.rangle - Math.PI/10;
        }
    }

    for (var i = 0;i<depdeu.length/2;i++){
        var r = depdeu[1+i*2];
        if(r.hig){
            r.angle = r.angle - Math.PI/10;
            r.inverse = r.inverse - Math.PI/10;
            r.rangle = r.rangle - Math.PI/10;
        }
    }
    draw();
}

function tournerdroite(){
    for (var i = 0; i < adversaire.length; i++) {
        var r = adversaire[i];
        if(r.hig){
            r.angle = r.angle + Math.PI/10;
            r.inverse = r.inverse + Math.PI/10;
            r.rangle = r.rangle + Math.PI/10;
        }
    }
    for (var i = 0;i<deple.length/4;i++){
        var r = deple[3+i*4];
        if(r.hig){
            r.angle = r.angle + Math.PI/10;
            r.inverse = r.inverse + Math.PI/10;
            r.rangle = r.rangle + Math.PI/10;
        }
    }

    for (var i = 0;i<depdeu.length/2;i++){
        var r = depdeu[1+i*2];
        if(r.hig){
            r.angle = r.angle + Math.PI/10;
            r.inverse = r.inverse + Math.PI/10;
            r.rangle = r.rangle + Math.PI/10;
        }
    }
    draw();
}

function showData (form) {
    var showData= document.getElementById("myTextBox");
    x = showData.value * (Math.PI/180);
    for (var i = 0; i < adversaire.length; i++) {
        var r = adversaire[i];
        if(r.hig){
            r.angle = x;
            r.inverse = Math.PI +x;
            r.rangle = x - Math.PI/2;
        }
    }
    for (var i = 0; i < deple.length/4; i++) {
        var r = deple[3+i*4];
        if(r.hig){
            r.angle = x;
            r.inverse = Math.PI +x;
            r.rangle = x - Math.PI/2;
        }
    }

    for (var i = 0; i < depdeu.length/2; i++) {
        var r = depdeu[1+i*2];
        if(r.hig){
            r.angle = x;
            r.inverse = Math.PI +x;
            r.rangle = x - Math.PI/2;
        }
    }
    draw();
}

function nextStep(){
    for(var i = 0;i < joueurs.length;i++){
        //console.log("zaeaz");
        var r = joueurs[i];
        for(var t =0 ; t< deple.length/4;t++){
            if(deple[0+t*4].jbase == i){
                r.x = deple[3+t*4].x;
                r.y = deple[3+t*4].y;
                deple.splice(0+t*4);
                deple.splice(1+t*4);
                deple.splice(2+t*4);
                deple.splice(3+t*4);
                nbptsdep = nbptsdep-4;
            }
        }
        for(var t =0 ; t< depdeu.length/2;t++){
            if(depdeu[0+t*2].jbase == i){
                r.x = depdeu[1+t*2].x;
                r.y = depdeu[1+t*2].y;
                depdeu.splice(0+t*2);
                depdeu.splice(1+t*2);
                nbdepdeu = nbdepdeu-2;
            }
        }
        for(var t =0 ; t< dribblement.length/4;t++){
            if(dribblement[0+t*4].dbase == i){
                r.x = dribblement[3+t*4].x;
                r.y = dribblement[3+t*4].y;
                dribblement.splice(0+t*4);
                dribblement.splice(1+t*4);
                dribblement.splice(2+t*4);
                dribblement.splice(3+t*4);
                nbptsdib = nbptsdib-4;
            }
        }
        for(var t =0 ; t< drideu.length/2;t++){
            if(drideu[0+t*2].dbase == i){
                r.x = drideu[1+t*2].x;
                r.y = drideu[1+t*2].y;
                drideu.splice(0+t*2);
                drideu.splice(1+t*2);
                nbdrideu = nbdrideu-2;
            }
        }

        if(r.setball == true){
            r.gottheball = true;
        }
        else{
            r.gottheball = false;
        }
        r.tiring = false;
    }
    for(var t = 0; t< passes.length+1;t++){
        passes.pop();
    }

    draw();

}

function centreIntervalle(xa, ya, xb, yb, nbIntervalle, intervalles,abon) {
    // Tableau des coordonnées des points étant au centre de chaque intervalle :
    centreI = [];

    let dernierx;
    let derniery;

    let tailleTab = intervalles.length;
    //console.log("Emplacement [0] : ", intervalles[0]);

    //Calcul de chaque distance B - Intervalle :
    for(let i = 0; i < tailleTab; i = i + 4) {
        let dist1 = Math.sqrt(Math.pow((intervalles[i] - xb), 2) + Math.pow((intervalles[i + 1] - yb), 2));
        let dist2 = Math.sqrt(Math.pow((intervalles[i + 2] - xb), 2) + Math.pow((intervalles[i + 3] - yb), 2));
        if(dist1 < dist2) {
            centreI.push(intervalles[i], intervalles[i + 1]);
        }
        if(dist2 < dist1) {
            centreI.push(intervalles[i + 2], intervalles[i + 3]);
        }
    }
    let tailleTab2 = centreI.length;

    pointsATracer = [];

    let aab = (ya - yb)/(xa - xb);

    // cas où les points ont la même ordonnée
    if (aab == 0){
        aab = 0.001;
    }

    //coefficient directeur de DE : 
    let ade = -1/aab;

    // Distance entre C et E :
    let CE = 8;

    for(let i = 0; i <= tailleTab2; i = i + 2) {
        if(i == 0) {
            //Coordonnées de C, milieu de l'intervalle :
            var xc = (xa + centreI[0])/2;
            var yc = (ya + centreI[1])/2;
        }

        if(i > 0) {
            //Coordonnées de C, milieu de l'intervalle :
            var xc = (centreI[i - 2] + centreI[i])/2;
            var yc = (centreI[i - 1] + centreI[i + 1])/2;
        }

        if(i == tailleTab2) {
            //Coordonnées de C, milieu de l'intervalle :
            var xc = (centreI[i - 2] + xb)/2;
            var yc = (centreI[i - 1] + yb)/2;
        }

        // Ordonnée à l'origine de DE :
        var bde = yc - ade*xc;

        let A = (1 + Math.pow(ade, 2));
        let B = (2*ade*bde - 2*ade*yc - 2*xc);
        let C = (Math.pow(xc, 2) + Math.pow(bde, 2) + Math.pow(yc, 2) - 2*bde*yc - Math.pow(CE, 2));

        // Delta :
        let D = (Math.pow(B, 2) - 4*A*C);

        // Abscisses de E et de D :
        let xe = (-B + Math.sqrt(D))/(2*A);
        let xd = (-B - Math.sqrt(D))/(2*A);

        // Ordonnées de E et de D :
        let ye = ade*xe + bde;
        let yd = ade*xd + bde;

        pointsATracer.push(xd, yd, xe, ye);

    }

    let taillePointsATracer = pointsATracer.length;
    let tailleCentreI = centreI.length;
    ctx.beginPath();
    ctx.setLineDash([]);
    ctx.strokeStyle = "black";

    ctx.moveTo(xa, ya);
    ctx.lineTo(pointsATracer[0], pointsATracer[1]);

    
    if(abon){
        for(let i = 0; i <= taillePointsATracer - 8; i = i + 8) {
            ctx.moveTo(pointsATracer[i], pointsATracer[i + 1]);
            ctx.lineTo(pointsATracer[i + 6], pointsATracer[i + 7]);
            ctx.lineTo(pointsATracer[i + 8], pointsATracer[i + 9]);
            dernierx = pointsATracer[i + 8];
            derniery = pointsATracer[i + 9];
        }
        ctx.moveTo(dernierx, derniery);
        ctx.lineTo(xb, yb);
        ctx.stroke();

    }

    else{

        for(let i = 0; i < taillePointsATracer - 16; i = i + 8) {
            ctx.moveTo(pointsATracer[i], pointsATracer[i + 1]);
            ctx.lineTo(pointsATracer[i + 6], pointsATracer[i + 7]);
            ctx.moveTo(pointsATracer[i + 6], pointsATracer[i + 7]);
            ctx.lineTo(pointsATracer[i + 8], pointsATracer[i + 9]);
        }
        ctx.lineTo(centreI[tailleCentreI - 4], centreI[tailleCentreI - 3]);
        ctx.moveTo(centreI[tailleCentreI - 4], centreI[tailleCentreI - 3]);
        ctx.lineTo(xb, yb);
        ctx.stroke();
    }
}
//Fonction permettant de calculer tous les points d'intervalles sur la droite
function intervalleDroite(xa, ya, xb, yb) {
    //coefficient directeur de AB :
    let aab = (ya - yb)/(xa - xb);

    //cas où les points ont la même abscisse :
    if (aab == Infinity){
        aab = 2000; //on simule un très grand coefficient directeur pour éviter d'avoir une valeur infinie
    }
    if (aab == -Infinity){
        aab = -2000; //on simule un très petit coefficient directeur pour éviter d'avoir une valeur infiniment petite
    }

    // Distance entre A et B :
    let AB = Math.sqrt(Math.pow((xb - xa), 2) + Math.pow((yb - ya), 2));

    //Le nombre d'intervalle est proportionnel à la distance AB
    nbIntervalle = Math.round(AB/10);
    //nbIntervalle = Math.round(nbIntervalle);

    // Distance entre deux intervalles :
    let dist = AB/(nbIntervalle + 1);

    // Ordonnée à l'origine de AB :
    var bab = ya - aab*xa;

    // Tableau des coordonnées des intervalles :
    var intervalles = [];

    for(let i = 1; i <= nbIntervalle; i++) {
        // Coordonnées intervalles :
        let Ai = (1 + Math.pow(aab, 2));
        let Bi = (2*aab*bab - 2*aab*ya - 2*xa);
        let Ci = (Math.pow(xa, 2) + Math.pow(bab, 2) + Math.pow(ya, 2) - 2*bab*ya - Math.pow(dist*i, 2));

        // Delta :
        let Di = (Math.pow(Bi, 2) - 4*Ai*Ci);

        // Abscisses de Ei et de Di :
        let xi1 = (-Bi + Math.sqrt(Di))/(2*Ai);
        let xi2 = (-Bi - Math.sqrt(Di))/(2*Ai);

        // Ordonnées de Ei et de Di :
        let yi1 = aab*xi1 + bab;
        let yi2 = aab*xi2 + bab;

        intervalles.push(xi1, yi1, xi2, yi2);
    }
    //console.log("Longueur tableau : ", intervalles.length);
    // intervalles.forEach(element => console.log(element));
    return intervalles;
}

function toggleOutilAppui(){
    document.getElementById("gauche").classList.remove('yay');
    document.getElementById("droite").classList.remove('yay');
    document.getElementById("perso").classList.remove('yay');

}

function UntoggleOutilAppui(){
    document.getElementById("gauche").classList.add('yay');
    document.getElementById("droite").classList.add('yay');
    document.getElementById("perso").classList.add('yay');

}

function togglePasse(){
    document.getElementById("bouttonpasse").classList.add('red');

}
function untogglePasse(){
    document.getElementById("bouttonpasse").classList.remove('red');

}
function toggleEffacer(){
    document.getElementById("bouttoneffacer").classList.add('red');

}
function untoggleEffacer(){
    document.getElementById("bouttoneffacer").classList.remove('red');

}
function toggleDeplacer(){
    document.getElementById("bouttondeplacer").classList.add('red');

}
function untoggleDeplacer(){
    document.getElementById("bouttondeplacer").classList.remove('red');

}

function toggleDribble(){
    document.getElementById("bouttondribbler").classList.add('red');

}

function untoggleDribble(){
    document.getElementById("bouttondribbler").classList.remove('red');

}

function toggleDonner(){
    document.getElementById("bouttondonner").classList.add('red');

}

function untoggleDonner(){
    document.getElementById("bouttondonner").classList.remove('red');

}

function togglezone(){
    document.getElementById("bouttoninfluence").classList.add('red');

}
function untogglezone(){
    document.getElementById("bouttoninfluence").classList.remove('red');

}
function toggleediter(){
    document.getElementById("bouttonediter").classList.add('red');

}
function untoggleediter(){
    document.getElementById("bouttonediter").classList.remove('red');

}

function toggletire(){
    document.getElementById("bouttontirer").classList.add('red');

}
function untoggletire(){
    document.getElementById("bouttontirer").classList.remove('red');

}

function toggleecran(){
    document.getElementById("bouttonecran").classList.add('red');

}
function untoggleecran(){
    document.getElementById("bouttonecran").classList.remove('red');

}
buttonPNG.onclick = function(){
    download(canvas, 'Schema.png');
}




function downloadpng() {
  /// create an "off-screen" anchor tag
  var image = document.createElement('a'), e;
  let nomFich = prompt("Veuillez entrer le nom du schéma", "");
  if (nomFich == null || nomFich == "")
    nomFich ="Schéma";
le_contexte.beginPath();
le_contexte.setLineDash([]);
le_contexte.font = '35px serif';
ctx.fillStyle="black";
if (joueurs.length == 0) {
    le_contexte.fillText(nomFich, Math.round(canvas.width*0.45),Math.round(canvas.height*0.98));
}
else if(joueurs.length != 0)
    le_contexte.fillText(nomFich, Math.round(canvas.width*0.45),Math.round(canvas.height*0.95));
le_contexte.stroke();
nomFich =nomFich+".png";
image.download = nomFich;

image.href = canvas.toDataURL("image/png;base64");

if (document.createEvent) {
    e = document.createEvent("MouseEvents");
    e.initMouseEvent("click", true, true, window,
       0, 0, 0, 0, 0, false, false, false,
       false, 0, null);

    image.dispatchEvent(e);
    draw();
} else if (lnk.fireEvent) {
    image.fireEvent("onclick");
    draw();
}
}


buttonPNG.onclick = function(){
    downloadpng();
}




downloadpdf();


function downloadpdf() {

    var button = document.getElementById( 'downloadpdf' );
    button.addEventListener( 'click', function() {
        var imgData = canvas.toDataURL("image/jpeg", 1.0);
        var pdf = new jsPDF('1', 'px', [canvas.width*0.75, canvas.height*0.85]);

        pdf.addImage(imgData, 'JPEG',0 ,0);


        let nom = prompt("Veuillez entrer le nom du schéma", "");
        if (nom == null || nom == "")
            nom ="Schéma";

        pdf.setFontSize(22);
        pdf.text(280,370,nom);
        pdf.save(nom+".pdf");

    } );
}


function ecraner(){


  if(ecra ){
    ecra = false;
    untoggleecran();
}
else{
    ecra = true;
    toggleecran();
}
draw();

}


function togglePopup(){
    document.getElementById("popup-2").classList.toggle("inactive");
    document.getElementById("popup-3").classList.toggle("inactive");
    document.getElementById("popup-4").classList.toggle("inactive");
    document.getElementById("popup-5").classList.toggle("inactive");  
    document.getElementById("popup-1").classList.toggle("active");
    document.onmousedown=calcul;

}
function togglePopup2(){
  document.getElementById("popup-1").classList.toggle("inactive");
  document.getElementById("popup-3").classList.toggle("inactive");
  document.getElementById("popup-4").classList.toggle("inactive");
  document.getElementById("popup-5").classList.toggle("inactive");    
  document.getElementById("popup-2").classList.toggle("active");
  document.onmousedown=calcul2;
}

function togglePopup3(){
  document.getElementById("popup-1").classList.toggle("inactive");
  document.getElementById("popup-2").classList.toggle("inactive");
  document.getElementById("popup-4").classList.toggle("inactive");
  document.getElementById("popup-5").classList.toggle("inactive");      
  document.getElementById("popup-3").classList.toggle("active");
  document.onmousedown=calcul3;
}

function togglePopup4(){
  document.getElementById("popup-1").classList.toggle("inactive");
  document.getElementById("popup-2").classList.toggle("inactive");
  document.getElementById("popup-3").classList.toggle("inactive");
  document.getElementById("popup-5").classList.toggle("inactive");   
  document.getElementById("popup-4").classList.toggle("active");
  document.onmousedown=calcul4;
}


function togglePopup5(){
  document.getElementById("popup-1").classList.toggle("inactive");
  document.getElementById("popup-2").classList.toggle("inactive");
  document.getElementById("popup-3").classList.toggle("inactive");
  document.getElementById("popup-4").classList.toggle("inactive");   
  document.getElementById("popup-5").classList.toggle("active");
  document.onmousedown=calcul5;
}
function togglePopup6(){
  document.getElementById("popup-1").classList.toggle("inactive");
  document.getElementById("popup-2").classList.toggle("inactive");
  document.getElementById("popup-3").classList.toggle("inactive");
  document.getElementById("popup-4").classList.toggle("inactive");
  document.getElementById("popup-5").classList.toggle("inactive");
  document.getElementById("popup-7").classList.toggle("inactive");
  document.getElementById("popup-8").classList.toggle("inactive");
  document.getElementById("popup-6").classList.toggle("active");

  document.onmousedown=calcul6;
}

function togglePopup7(){
  document.getElementById("popup-1").classList.toggle("inactive");
  document.getElementById("popup-2").classList.toggle("inactive");
  document.getElementById("popup-3").classList.toggle("inactive");
  document.getElementById("popup-4").classList.toggle("inactive");
  document.getElementById("popup-5").classList.toggle("inactive");
  document.getElementById("popup-6").classList.toggle("inactive");
  document.getElementById("popup-8").classList.toggle("inactive");
  document.getElementById("popup-7").classList.toggle("active");
  document.onmousedown=calcul7;

}

function togglePopup8(){
  document.getElementById("popup-1").classList.toggle("inactive");
  document.getElementById("popup-2").classList.toggle("inactive");
  document.getElementById("popup-3").classList.toggle("inactive");
  document.getElementById("popup-4").classList.toggle("inactive");
  document.getElementById("popup-5").classList.toggle("inactive");
  document.getElementById("popup-6").classList.toggle("inactive");
  document.getElementById("popup-7").classList.toggle("inactive");
  document.getElementById("popup-8").classList.toggle("active");

  document.onmousedown=calcul8;
}
function togglePopupTaille(){
  document.getElementById("popup-taille").classList.toggle("active");
}
function togglePopupligne(){
  document.getElementById("popup-ligne").classList.toggle("active");
}


function togglePopupTa(){
  document.getElementById("popup-6").classList.toggle("active");
}
function togglePopup2Td(){
  document.getElementById("popup-7").classList.toggle("active");
}

var clic=false; 
//Cette variable nous indique si l'utilisateur clique sur la barre.

var clic2=false;
//Cette variable nous indique si l'utilisateur clique sur le carré.
var blanc=0,noir=1;
// Le pourcentage de noir et de blanc entre 0 et 1 appliqué à la couleur (ici, pour le noir, 1 signifie qu'il n'y en aura pas, et 0 totalement : c'est l'inverse)

var x=360,y=20;
var posxDef=360,posyDef=20;
var posxTer=360,posyTer=20;
var posxinTer=360,posyinTer=20;
var posxzone=360,posyzone=20;
//position initiale de curseur2 (dans le carré).


function clique(objet)
{

    if(objet=="barre") // si l'utilisateur clique sur la barre ...
    {   
        clic=true; // ...alors on met true (vrai) à clic
    }
    else // sinon l'utilisateur clique sur le carré ...
    { 
        clic2=true; // ...alors on met true (vrai) à clic2
    }

}

function position(axe,event)
{
    // event contient les évènements de la page (on s'en sert pour la position du curseur)

    var e = event || window.event; 
    // on copie les évènements dans e : il y a des manières différentes de récupérer les événements selon le navigateur

        if(axe=="x") //si on demande x
        {
            var rep=e.clientX; // position x de la souris.
        }
        else // sinon y 
        {
            var rep=e.clientY; // position y de la souris.
        }

        return rep;
    // on renvoie la valeur de rep.

}

function position2(axe,event)
{
    // event contient les évènements de la page (on s'en sert pour la position du curseur)

    var e = event || window.event; 
    // on copie les évènements dans e : il y a des manières différentes de récupérer les événements selon le navigateur

        if(axe=="posxDef") //si on demande x
        {
            var rep=e.clientX; // position x de la souris.
        }
        else // sinon y 
        {
            var rep=e.clientY; // position y de la souris.
        }

        return rep;
    // on renvoie la valeur de rep.

}
function position3(axe,event)
{
    // event contient les évènements de la page (on s'en sert pour la position du curseur)

    var e = event || window.event; 
    // on copie les évènements dans e : il y a des manières différentes de récupérer les événements selon le navigateur

        if(axe=="posxTer") //si on demande x
        {
            var rep=e.clientX; // position x de la souris.
        }
        else // sinon y 
        {
            var rep=e.clientY; // position y de la souris.
        }

        return rep;
    // on renvoie la valeur de rep.

}
function position4(axe,event)
{
    // event contient les évènements de la page (on s'en sert pour la position du curseur)

    var e = event || window.event; 
    // on copie les évènements dans e : il y a des manières différentes de récupérer les événements selon le navigateur

        if(axe=="posxinTer") //si on demande x
        {
            var rep=e.clientX; // position x de la souris.
        }
        else // sinon y 
        {
            var rep=e.clientY; // position y de la souris.
        }

        return rep;
    // on renvoie la valeur de rep.

}

function position5(axe,event)
{
    // event contient les évènements de la page (on s'en sert pour la position du curseur)

    var e = event || window.event; 
    // on copie les évènements dans e : il y a des manières différentes de récupérer les événements selon le navigateur

        if(axe=="posxzone") //si on demande x
        {
            var rep=e.clientX; // position x de la souris.
        }
        else // sinon y 
        {
            var rep=e.clientY; // position y de la souris.
        }

        return rep;
    // on renvoie la valeur de rep.

}

function position6(axe,event)
{
    // event contient les évènements de la page (on s'en sert pour la position du curseur)

    var e = event || window.event; 
    // on copie les évènements dans e : il y a des manières différentes de récupérer les événements selon le navigateur

        if(axe=="posxmouv") //si on demande x
        {
            var rep=e.clientX; // position x de la souris.
        }
        else // sinon y 
        {
            var rep=e.clientY; // position y de la souris.
        }

        return rep;
    // on renvoie la valeur de rep.

}

function position7(axe,event)
{
    // event contient les évènements de la page (on s'en sert pour la position du curseur)

    var e = event || window.event; 
    // on copie les évènements dans e : il y a des manières différentes de récupérer les événements selon le navigateur

        if(axe=="posxtouche") //si on demande x
        {
            var rep=e.clientX; // position x de la souris.
        }
        else // sinon y 
        {
            var rep=e.clientY; // position y de la souris.
        }

        return rep;
    // on renvoie la valeur de rep.

}
function position8(axe,event)
{
    // event contient les évènements de la page (on s'en sert pour la position du curseur)

    var e = event || window.event; 
    // on copie les évènements dans e : il y a des manières différentes de récupérer les événements selon le navigateur

        if(axe=="posxligne") //si on demande x
        {
            var rep=e.clientX; // position x de la souris.
        }
        else // sinon y 
        {
            var rep=e.clientY; // position y de la souris.
        }

        return rep;
    // on renvoie la valeur de rep.

}

// lorsque la souris bouge n'importe où dans le document, on appelle la fonction calcul.

// lorsque la souris clique n'importe où dans le document, on appelle la fonction calcul.

document.onmouseup=function() { clic=false;clic2=false; }
// si l'utilisateur relâche le bouton de la souris, alors les variables clic et clic2 redeviennent fausses (false).


function calcul(event) // event contient les événements de la page (on s'en sert pour la position du curseur).
{

    if(clic && position('y',event)<=320 && position('y',event)>=20) // on appelle position() pour connaître la position de la souris.
    {
        document.getElementById("curseur1").style.top=position('y',event)-7;
    //on change la position du curseur (top) en même temps que la souris.


    // c'est à partir d'ici qu'on regarde sur quel sixième la souris se trouve.

    if((position('y',event)-20)<=50) // 1/6 (50px)
    {

        r=255;
        g=0;
        b=Math.round((position('y',event)-20)*255/50);

    }
    else if((position('y',event)-20)<=100) // 2/6 (100px)
    {

        r=Math.round(255-((position('y',event)-70)*255/50));
        g=0;
        b=255;

    }
    else if((position('y',event)-20)<=150) // 3/6 (150px)
    {

        r=0;
        g=Math.round((position('y',event)-120)*255/50);
        b=255;

    }
    else if((position('y',event)-20)<=200) // 4/6 (200px)
    {

        r=0;
        g=255;
        b=Math.round(255-((position('y',event)-170)*255/50));

    }
    else if((position('y',event)-20)<=250) // 5/6 (250px)
    {

        r=Math.round((position('y',event)-220)*255/50);
        g=255;
        b=0;

    }
    else if((position('y',event)-20)<=300) // 6/6 (300px)
    {

        r=255;
        g=Math.round(255-((position('y',event)-270)*255/50));
        b=0;

    }

    document.getElementById("carre").style.backgroundColor="rgb("+r+","+g+","+b+")";
    // On change la couleur du carré. On voit après (grâce à degrade n-b.png) le dégradé de la couleur vers le blanc et le noir.




        afficher(); // fonction permettant d'afficher la couleur courante dans le rectangle (input text) 'resultat'.
    }

    if(clic2) // si l'utilisateur clique sur le carré...
    {

        if(position('y',event)>20 && position('y',event)<320)
        { 
            document.getElementById("curseur2").style.top=(position('y',event)-10)+"px";
// on déplace curseur2 et on lui retire son milieu (comme pour curseur 1)

y=position('y',event);
// on enregistre la position y de la souris dans la variable 'y' pour que la fonction afficher() puisse faire ces calculs.
}

if(position('x',event)>60 && position('x',event)<360)
{
    document.getElementById("curseur2").style.left=(position('x',event)-10)+"px";
// on déplace curseur2 et on lui retire son milieu (comme pour curseur 1)

x=position('x',event);
// on enregistre la position x de la souris dans la variable 'x' pour que la fonction afficher() puisse faire ces calculs.
}

        afficher(); // fonction permettant d'afficher la couleur courante dans le rectangle (input text) 'resultat'.
    }

}


function calcul2(event) // event contient les événements de la page (on s'en sert pour la position du curseur).
{

    if(clic && position2('posyDef',event)<=320 && position2('posyDef',event)>=20) // on appelle position() pour connaître la position de la souris.
    {
        document.getElementById("curseur1def").style.top=position2('posyDef',event)-7;
    //on change la position du curseur (top) en même temps que la souris.


    // c'est à partir d'ici qu'on regarde sur quel sixième la souris se trouve.

    if((position2('posyDef',event)-20)<=50) // 1/6 (50px)
    {

        r2=255;
        g2=0;
        b2=Math.round((position2('posyDef',event)-20)*255/50);

    }
    else if((position2('posyDef',event)-20)<=100) // 2/6 (100px)
    {

        r2=Math.round(255-((position2('posyDef',event)-70)*255/50));
        g2=0;
        b2=255;

    }
    else if((position2('posyDef',event)-20)<=150) // 3/6 (150px)
    {

        r2=0;
        g2=Math.round((position2('posyDef',event)-120)*255/50);
        b2=255;

    }
    else if((position2('posyDef',event)-20)<=200) // 4/6 (200px)
    {

        r2=0;
        g2=255;
        b2=Math.round(255-((position2('posyDef',event)-170)*255/50));

    }
    else if((position2('posyDef',event)-20)<=250) // 5/6 (250px)
    {

        r2=Math.round((position2('posyDef',event)-220)*255/50);
        g2=255;
        b2=0;

    }
    else if((position2('posyDef',event)-20)<=300) // 6/6 (300px)
    {

        r2=255;
        g2=Math.round(255-((position2('posyDef',event)-270)*255/50));
        b2=0;

    }

    document.getElementById("lecarre").style.backgroundColor="rgb("+r2+","+g2+","+b2+")";
    // On change la couleur du carré. On voit après (grâce à degrade n-b.png) le dégradé de la couleur vers le blanc et le noir.




        afficher2(); // fonction permettant d'afficher la couleur courante dans le rectangle (input text) 'resultat'.
    }

    if(clic2) // si l'utilisateur clique sur le carré...
    {

        if(position2('posyDef',event)>20 && position2('posyDef',event)<320)
        { 
            document.getElementById("curseur2def").style.top=(position2('posyDef',event)-10)+"px";
// on déplace curseur2 et on lui retire son milieu (comme pour curseur 1)

posyDef=position2('posyDef',event);
// on enregistre la position y de la souris dans la variable 'y' pour que la fonction afficher() puisse faire ces calculs.
}

if(position2('posxDef',event)>60 && position2('posxDef',event)<360)
{
    document.getElementById("curseur2def").style.left=(position2('posxDef',event)-10)+"px";
// on déplace curseur2 et on lui retire son milieu (comme pour curseur 1)

posxDef=position2('posxDef',event);
// on enregistre la position x de la souris dans la variable 'x' pour que la fonction afficher() puisse faire ces calculs.
}

        afficher2(); // fonction permettant d'afficher la couleur courante dans le rectangle (input text) 'resultat'.
    }

}


function calcul3(event) // event contient les événements de la page (on s'en sert pour la position du curseur).
{

    if(clic && position3('posyTer',event)<=320 && position3('posyTer',event)>=20) // on appelle position() pour connaître la position de la souris.
    {
        document.getElementById("curseur1ter").style.top=position3('posyTer',event)-7;
    //on change la position du curseur (top) en même temps que la souris.


    // c'est à partir d'ici qu'on regarde sur quel sixième la souris se trouve.

    if((position3('posyTer',event)-20)<=50) // 1/6 (50px)
    {

        r3=255;
        g3=0;
        b3=Math.round((position3('posyTer',event)-20)*255/50);

    }
    else if((position3('posyTer',event)-20)<=100) // 2/6 (100px)
    {

        r3=Math.round(255-((position3('posyTer',event)-70)*255/50));
        g3=0;
        b3=255;

    }
    else if((position3('posyTer',event)-20)<=150) // 3/6 (150px)
    {

        r3=0;
        g3=Math.round((position3('posyTer',event)-120)*255/50);
        b3=255;

    }
    else if((position3('posyTer',event)-20)<=200) // 4/6 (200px)
    {

        r3=0;
        g3=255;
        b3=Math.round(255-((position3('posyTer',event)-170)*255/50));

    }
    else if((position3('posyTer',event)-20)<=250) // 5/6 (250px)
    {

        r3=Math.round((position3('posyTer',event)-220)*255/50);
        g3=255;
        b3=0;

    }
    else if((position3('posyTer',event)-20)<=300) // 6/6 (300px)
    {

        r3=255;
        g3=Math.round(255-((position3('posyTer',event)-270)*255/50));
        b3=0;

    }

    document.getElementById("lecarret").style.backgroundColor="rgb("+r3+","+g3+","+b3+")";
    // On change la couleur du carré. On voit après (grâce à degrade n-b.png) le dégradé de la couleur vers le blanc et le noir.




        afficher3(); // fonction permettant d'afficher la couleur courante dans le rectangle (input text) 'resultat'.
    }

    if(clic2) // si l'utilisateur clique sur le carré...
    {

        if(position3('posyTer',event)>20 && position3('posyTer',event)<320)
        { 
            document.getElementById("curseur2ter").style.top=(position3('posyTer',event)-10)+"px";
// on déplace curseur2 et on lui retire son milieu (comme pour curseur 1)

posyTer=position3('posyTer',event);
// on enregistre la position y de la souris dans la variable 'y' pour que la fonction afficher() puisse faire ces calculs.
}

if(position3('posxTer',event)>60 && position3('posxTer',event)<360)
{
    document.getElementById("curseur2ter").style.left=(position3('posxTer',event)-10)+"px";
// on déplace curseur2 et on lui retire son milieu (comme pour curseur 1)

posxTer=position3('posxTer',event);
// on enregistre la position x de la souris dans lposxTera variable 'x' pour que la fonction afficher() puisse faire ces calculs.
}

        afficher3(); // fonction permettant d'afficher la couleur courante dans le rectangle (input text) 'resultat'.
    }

}

function calcul4(event) // event contient les événements de la page (on s'en sert pour la position du curseur).
{

    if(clic && position4('posyinTer',event)<=320 && position4('posyinTer',event)>=20) // on appelle position() pour connaître la position de la souris.
    {
        document.getElementById("curseur1inter").style.top=position4('posyinTer',event)-7;
    //on change la position du curseur (top) en même temps que la souris.


    // c'est à partir d'ici qu'on regarde sur quel sixième la souris se trouve.

    if((position4('posyinTer',event)-20)<=50) // 1/6 (50px)
    {

        r4=255;
        g4=0;
        b4=Math.round((position4('posyinTer',event)-20)*255/50);

    }
    else if((position4('posyinTer',event)-20)<=100) // 2/6 (100px)
    {

        r4=Math.round(255-((position4('posyinTer',event)-70)*255/50));
        g4=0;
        b4=255;

    }
    else if((position4('posyinTer',event)-20)<=150) // 3/6 (150px)
    {

        r4=0;
        g4=Math.round((position4('posyinTer',event)-120)*255/50);
        b4=255;

    }
    else if((position4('posyinTer',event)-20)<=200) // 4/6 (200px)
    {

        r4=0;
        g4=255;
        b4=Math.round(255-((position4('posyinTer',event)-170)*255/50));

    }
    else if((position4('posyinTer',event)-20)<=250) // 5/6 (250px)
    {

        r4=Math.round((position4('posyinTer',event)-220)*255/50);
        g4=255;
        b4=0;

    }
    else if((position4('posyinTer',event)-20)<=300) // 6/6 (300px)
    {

        r4=255;
        g4=Math.round(255-((position4('posyinTer',event)-270)*255/50));
        b4=0;

    }

    document.getElementById("lecarreint").style.backgroundColor="rgb("+r4+","+g4+","+b4+")";
    // On change la couleur du carré. On voit après (grâce à degrade n-b.png) le dégradé de la couleur vers le blanc et le noir.




        afficher4(); // fonction permettant d'afficher la couleur courante dans le rectangle (input text) 'resultat'.
    }

    if(clic2) // si l'utilisateur clique sur le carré...
    {

        if(position4('posyinTer',event)>20 && position4('posyinTer',event)<320)
        { 
            document.getElementById("curseur2inter").style.top=(position4('posyinTer',event)-10)+"px";
// on déplace curseur2 et on lui retire son milieu (comme pour curseur 1)

posyinTer=position4('posyinTer',event);
// on enregistre la position y de la souris dans la variable 'y' pour que la fonction afficher() puisse faire ces calculs.
}

if(position4('posxinTer',event)>60 && position4('posxinTer',event)<360)
{
    document.getElementById("curseur2inter").style.left=(position4('posxinTer',event)-10)+"px";
// on déplace curseur2 et on lui retire son milieu (comme pour curseur 1)

posxinTer=position4('posxinTer',event);
// on enregistre la position x de la souris dans lposxTera variable 'x' pour que la fonction afficher() puisse faire ces calculs.
}

        afficher4(); // fonction permettant d'afficher la couleur courante dans le rectangle (input text) 'resultat'.
    }

}
function calcul5(event) // event contient les événements de la page (on s'en sert pour la position du curseur).
{

    if(clic && position5('posyzone',event)<=320 && position5('posyzone',event)>=20) // on appelle position() pour connaître la position de la souris.
    {
        document.getElementById("curseur1zone").style.top=position5('posyzone',event)-7;
    //on change la position du curseur (top) en même temps que la souris.


    // c'est à partir d'ici qu'on regarde sur quel sixième la souris se trouve.

    if((position5('posyzone',event)-20)<=50) // 1/6 (50px)
    {

        r5=255;
        g5=0;
        b5=Math.round((position5('posyzone',event)-20)*255/50);

    }
    else if((position5('posyzone',event)-20)<=100) // 2/6 (100px)
    {

        r5=Math.round(255-((position5('posyzone',event)-70)*255/50));
        g5=0;
        b5=255;

    }
    else if((position5('posyzone',event)-20)<=150) // 3/6 (150px)
    {

        r5=0;
        g5=Math.round((position5('posyzone',event)-120)*255/50);
        b5=255;

    }
    else if((position5('posyzone',event)-20)<=200) // 4/6 (200px)
    {

        r5=0;
        g5=255;
        b5=Math.round(255-((position5('posyzone',event)-170)*255/50));

    }
    else if((position5('posyzone',event)-20)<=250) // 5/6 (250px)
    {

        r5=Math.round((position5('posyzone',event)-220)*255/50);
        g5=255;
        b5=0;

    }
    else if((position5('posyzone',event)-20)<=300) // 6/6 (300px)
    {

        r5=255;
        g5=Math.round(255-((position5('posyzone',event)-270)*255/50));
        b5=0;

    }

    document.getElementById("lecarrezone").style.backgroundColor="rgb("+r5+","+g5+","+b5+")";
    // On change la couleur du carré. On voit après (grâce à degrade n-b.png) le dégradé de la couleur vers le blanc et le noir.




        afficher5(); // fonction permettant d'afficher la couleur courante dans le rectangle (input text) 'resultat'.
    }

    if(clic2) // si l'utilisateur clique sur le carré...
    {

        if(position5('posyzone',event)>20 && position5('posyzone',event)<320)
        { 
            document.getElementById("curseur2zone").style.top=(position5('posyzone',event)-10)+"px";
// on déplace curseur2 et on lui retire son milieu (comme pour curseur 1)

posyzone=position5('posyzone',event);
// on enregistre la position y de la souris dans la variable 'y' pour que la fonction afficher() puisse faire ces calculs.
}

if(position5('posxzone',event)>60 && position5('posxzone',event)<360)
{
    document.getElementById("curseur2zone").style.left=(position5('posxzone',event)-10)+"px";
// on déplace curseur2 et on lui retire son milieu (comme pour curseur 1)

posxzone=position5('posxzone',event);
// on enregistre la position x de la souris dans lposxTera variable 'x' pour que la fonction afficher() puisse faire ces calculs.
}

        afficher5(); // fonction permettant d'afficher la couleur courante dans le rectangle (input text) 'resultat'.
    }

}

function calcul6(event) // event contient les événements de la page (on s'en sert pour la position du curseur).
{

    console.log("la pos x touche au début de calcul" +posxmouv);
    console.log("la pos y touche au début de calcul"+posymouv);
    if(clic && position6('posymouv',event)<=320 && position6('posymouv',event)>=20) // on appelle position() pour connaître la position de la souris.
    {
        document.getElementById("curseur1mouv").style.top=position6('posymouv',event)-7;
    //on change la position du curseur (top) en même temps que la souris.


    // c'est à partir d'ici qu'on regarde sur quel sixième la souris se trouve.

    if((position6('posymouv',event)-20)<=50) // 1/6 (50px)
    {

        r6=255;
        g6=0;
        b6=Math.round((position6('posymouv',event)-20)*255/50);

    }
    else if((position6('posymouv',event)-20)<=100) // 2/6 (100px)
    {

        r6=Math.round(255-((position6('posymouv',event)-70)*255/50));
        g6=0;
        b6=255;

    }
    else if((position6('posymouv',event)-20)<=150) // 3/6 (150px)
    {

        r6=0;
        g6=Math.round((position6('posymouv',event)-120)*255/50);
        b6=255;

    }
    else if((position6('posymouv',event)-20)<=200) // 4/6 (200px)
    {

        r6=0;
        g6=255;
        b6=Math.round(255-((position6('posymouv',event)-170)*255/50));

    }
    else if((position6('posymouv',event)-20)<=250) // 5/6 (250px)
    {

        r6=Math.round((position6('posymouv',event)-220)*255/50);
        g6=255;
        b6=0;

    }
    else if((position6('posymouv',event)-20)<=300) // 6/6 (300px)
    {

        r6=255;
        g6=Math.round(255-((position6('posymouv',event)-270)*255/50));
        b6=0;

    }

    document.getElementById("lecarremouv").style.backgroundColor="rgb("+r6+","+g6+","+b6+")";
    // On change la couleur du carré. On voit après (grâce à degrade n-b.png) le dégradé de la couleur vers le blanc et le noir.




        afficher6(); // fonction permettant d'afficher la couleur courante dans le rectangle (input text) 'resultat'.
    }

    if(clic2) // si l'utilisateur clique sur le carré...
    {

        if(position6('posymouv',event)>20 && position6('posymouv',event)<320)
        { 
            document.getElementById("curseur2mouv").style.top=(position6('posymouv',event)-10)+"px";
// on déplace curseur2 et on lui retire son milieu (comme pour curseur 1)

posymouv=position6('posymouv',event);
// on enregistre la position y de la souris dans la variable 'y' pour que la fonction afficher() puisse faire ces calculs.
}

if(position6('posxmouv',event)>60 && position6('posxmouv',event)<360)
{
    document.getElementById("curseur2mouv").style.left=(position6('posxmouv',event)-10)+"px";
// on déplace curseur2 et on lui retire son milieu (comme pour curseur 1)

posxmouv=position6('posxmouv',event);
// on enregistre la position x de la souris dans lposxTera variable 'x' pour que la fonction afficher() puisse faire ces calculs.
}

        afficher6(); // fonction permettant d'afficher la couleur courante dans le rectangle (input text) 'resultat'.
    }

}

function calcul7(event) // event contient les événements de la page (on s'en sert pour la position du curseur).
{

    if(clic && position7('posytouche',event)<=320 && position7('posytouche',event)>=20) // on appelle position() pour connaître la position de la souris.
    {
        document.getElementById("curseur1touche").style.top=position7('posytouche',event)-7;
    //on change la position du curseur (top) en même temps que la souris.


    // c'est à partir d'ici qu'on regarde sur quel sixième la souris se trouve.

    if((position7('posytouche',event)-20)<=50) // 1/6 (50px)
    {

        r7=255;
        g7=0;
        b7=Math.round((position7('posytouche',event)-20)*255/50);

    }
    else if((position7('posytouche',event)-20)<=100) // 2/6 (100px)
    {

        r7=Math.round(255-((position7('posytouche',event)-70)*255/50));
        g7=0;
        b7=255;

    }
    else if((position7('posytouche',event)-20)<=150) // 3/6 (150px)
    {

        r7=0;
        g7=Math.round((position7('posytouche',event)-120)*255/50);
        b7=255;

    }
    else if((position7('posytouche',event)-20)<=200) // 4/6 (200px)
    {

        r7=0;
        g7=255;
        b7=Math.round(255-((position7('posytouche',event)-170)*255/50));

    }
    else if((position7('posytouche',event)-20)<=250) // 5/6 (250px)
    {

        r7=Math.round((position7('posytouche',event)-220)*255/50);
        g7=255;
        b7=0;

    }
    else if((position7('posytouche',event)-20)<=300) // 6/6 (300px)
    {

        r7=255;
        g7=Math.round(255-((position7('posytouche',event)-270)*255/50));
        b7=0;

    }

    document.getElementById("lecarretouche").style.backgroundColor="rgb("+r7+","+g7+","+b7+")";
    // On change la couleur du carré. On voit après (grâce à degrade n-b.png) le dégradé de la couleur vers le blanc et le noir.




        afficher7(); // fonction permettant d'afficher la couleur courante dans le rectangle (input text) 'resultat'.
    }

    if(clic2) // si l'utilisateur clique sur le carré...
    {

        if(position7('posytouche',event)>20 && position7('posytouche',event)<320)
        { 
            document.getElementById("curseur2touche").style.top=(position7('posytouche',event)-10)+"px";
// on déplace curseur2 et on lui retire son milieu (comme pour curseur 1)

posytouche=position7('posytouche',event);
// on enregistre la position y de la souris dans la variable 'y' pour que la fonction afficher() puisse faire ces calculs.
}

if(position7('posxtouche',event)>60 && position7('posxtouche',event)<360)
{
    document.getElementById("curseur2touche").style.left=(position7('posxtouche',event)-10)+"px";
// on déplace curseur2 et on lui retire son milieu (comme pour curseur 1)

posxtouche=position7('posxtouche',event);
// on enregistre la position x de la souris dans lposxTera variable 'x' pour que la fonction afficher() puisse faire ces calculs.
}

        afficher7(); // fonction permettant d'afficher la couleur courante dans le rectangle (input text) 'resultat'.
    }

}
function calcul8(event) // event contient les événements de la page (on s'en sert pour la position du curseur).
{

    if(clic && position8('posyligne',event)<=320 && position8('posyligne',event)>=20) // on appelle position() pour connaître la position de la souris.
    {
        document.getElementById("curseur1ligne").style.top=position8('posyligne',event)-7;
    //on change la position du curseur (top) en même temps que la souris.


    // c'est à partir d'ici qu'on regarde sur quel sixième la souris se trouve.

    if((position8('posyligne',event)-20)<=50) // 1/6 (50px)
    {

        r8=255;
        g8=0;
        b8=Math.round((position8('posyligne',event)-20)*255/50);

    }
    else if((position8('posyligne',event)-20)<=100) // 2/6 (100px)
    {

        r8=Math.round(255-((position8('posyligne',event)-70)*255/50));
        g8=0;
        b8=255;

    }
    else if((position8('posyligne',event)-20)<=150) // 3/6 (150px)
    {

        r8=0;
        g8=Math.round((position8('posyligne',event)-120)*255/50);
        b8=255;

    }
    else if((position8('posyligne',event)-20)<=200) // 4/6 (200px)
    {

        r8=0;
        g8=255;
        b8=Math.round(255-((position8('posyligne',event)-170)*255/50));

    }
    else if((position8('posyligne',event)-20)<=250) // 5/6 (250px)
    {

        r8=Math.round((position8('posyligne',event)-220)*255/50);
        g8=255;
        b8=0;

    }
    else if((position8('posyligne',event)-20)<=300) // 6/6 (300px)
    {

        r8=255;
        g8=Math.round(255-((position8('posyligne',event)-270)*255/50));
        b8=0;

    }

    document.getElementById("lecarreligne").style.backgroundColor="rgb("+r8+","+g8+","+b8+")";
    // On change la couleur du carré. On voit après (grâce à degrade n-b.png) le dégradé de la couleur vers le blanc et le noir.




        afficher8(); // fonction permettant d'afficher la couleur courante dans le rectangle (input text) 'resultat'.
    }

    if(clic2) // si l'utilisateur clique sur le carré...
    {

        if(position8('posyligne',event)>20 && position8('posyligne',event)<320)
        { 
            document.getElementById("curseur2ligne").style.top=(position8('posyligne',event)-10)+"px";
// on déplace curseur2 et on lui retire son milieu (comme pour curseur 1)

posyligne=position8('posyligne',event);
// on enregistre la position y de la souris dans la variable 'y' pour que la fonction afficher() puisse faire ces calculs.
}

if(position8('posxligne',event)>60 && position8('posxligne',event)<360)
{
    document.getElementById("curseur2ligne").style.left=(position8('posxligne',event)-10)+"px";
// on déplace curseur2 et on lui retire son milieu (comme pour curseur 1)

posxligne=position8('posxligne',event);
// on enregistre la position x de la souris dans lposxTera variable 'x' pour que la fonction afficher() puisse faire ces calculs.
}

        afficher8(); // fonction permettant d'afficher la couleur courante dans le rectangle (input text) 'resultat'.
    }

}


function afficher()  // ici on gère l'affichage de la couleur
{ 
    noir=Math.round( (x-60)*100/300)/100; 
    blanc=Math.round((y-20)*100/300)/100; 

    r_1=Math.round((255-r)*blanc)+r;
    g_1=Math.round((255-g)*blanc)+g;
    b_1=Math.round((255-b)*blanc)+b;

    r_1=Math.round(r_1*noir);
    g_1=Math.round(g_1*noir);
    b_1=Math.round(b_1*noir);

    // --modification--

    r_hexa = hexadecimal( Math.floor(r_1 /16) ) + hexadecimal( r_1 % 16 );
    g_hexa = hexadecimal( Math.floor(g_1 /16) ) + hexadecimal( g_1 % 16 );
    b_hexa = hexadecimal( Math.floor(b_1 /16) ) + hexadecimal( b_1 % 16 );

    document.getElementById("resultat").value = "#" + r_hexa + g_hexa + b_hexa;

    // --fin modification--

    document.getElementById('resultat').style.backgroundColor="rgb("+r_1+","+g_1+","+b_1+")";
    draw();


}
function afficher2()  // ici on gère l'affichage de la couleur
{ 
    noir=Math.round((posxDef-60)*100/300)/100; 
    blanc=Math.round((posyDef-20)*100/300)/100; 


    r_2=Math.round((255-r2)*blanc)+r2;
    g_2=Math.round((255-g2)*blanc)+g2;
    b_2=Math.round((255-b2)*blanc)+b2;

    r_2=Math.round(r_2*noir);
    g_2=Math.round(g_2*noir);
    b_2=Math.round(b_2*noir);

    // --modification--

    r_hexa2 = hexadecimal( Math.floor(r_2 /16) ) + hexadecimal( r_2 % 16 );
    g_hexa2 = hexadecimal( Math.floor(g_2 /16) ) + hexadecimal( g_2 % 16 );
    b_hexa2 = hexadecimal( Math.floor(b_2 /16) ) + hexadecimal( b_2 % 16 );

    document.getElementById("resultatdef").value = "#" + r_hexa2 + g_hexa2 + b_hexa2;
    // --fin modification--

    document.getElementById('resultatdef').style.backgroundColor="rgb("+r_2+","+g_2+","+b_2+")";
    draw();


}

function afficher3()  // ici on gère l'affichage de la couleur
{ 
    noir=Math.round((posxTer-60)*100/300)/100; 
    blanc=Math.round((posyTer-20)*100/300)/100; 


    r_3=Math.round((255-r3)*blanc)+r3;
    g_3=Math.round((255-g3)*blanc)+g3;
    b_3=Math.round((255-b3)*blanc)+b3;

    r_3=Math.round(r_3*noir);
    g_3=Math.round(g_3*noir);
    b_3=Math.round(b_3*noir);

    // --modification--

    r_hexa3 = hexadecimal( Math.floor(r_3 /16) ) + hexadecimal( r_3 % 16 );
    g_hexa3 = hexadecimal( Math.floor(g_3 /16) ) + hexadecimal( g_3 % 16 );
    b_hexa3 = hexadecimal( Math.floor(b_3 /16) ) + hexadecimal( b_3 % 16 );

    document.getElementById("resultatter").value = "#" + r_hexa3 + g_hexa3 + b_hexa3;
    // --fin modification--

    document.getElementById('resultatter').style.backgroundColor="rgb("+r_3+","+g_3+","+b_3+")";
    draw();


}
function afficher4()  // ici on gère l'affichage de la couleur
{ 
    noir=Math.round((posxinTer-60)*100/300)/100; 
    blanc=Math.round((posyinTer-20)*100/300)/100; 


    r_4=Math.round((255-r4)*blanc)+r4;
    g_4=Math.round((255-g4)*blanc)+g4;
    b_4=Math.round((255-b4)*blanc)+b4;

    r_4=Math.round(r_4*noir);
    g_4=Math.round(g_4*noir);
    b_4=Math.round(b_4*noir);

    // --modification--

    r_hexa4 = hexadecimal( Math.floor(r_4 /16) ) + hexadecimal( r_4 % 16 );
    g_hexa4 = hexadecimal( Math.floor(g_4 /16) ) + hexadecimal( g_4 % 16 );
    b_hexa4 = hexadecimal( Math.floor(b_4 /16) ) + hexadecimal( b_4 % 16 );

    document.getElementById("resultatinter").value = "#" + r_hexa4 + g_hexa4 + b_hexa4;
    // --fin modification--

    document.getElementById('resultatinter').style.backgroundColor="rgb("+r_4+","+g_4+","+b_4+")";
    draw();


}

function afficher5()  // ici on gère l'affichage de la couleur
{ 
    noir=Math.round((posxzone-60)*100/300)/100; 
    blanc=Math.round((posyzone-20)*100/300)/100; 


    r_5=Math.round((255-r5)*blanc)+r5;
    g_5=Math.round((255-g5)*blanc)+g5;
    b_5=Math.round((255-b5)*blanc)+b5;

    r_5=Math.round(r_5*noir);
    g_5=Math.round(g_5*noir);
    b_5=Math.round(b_5*noir);

    // --modification--

    r_hexa5 = hexadecimal( Math.floor(r_5 /16) ) + hexadecimal( r_5 % 16 );
    g_hexa5 = hexadecimal( Math.floor(g_5 /16) ) + hexadecimal( g_5 % 16 );
    b_hexa5 = hexadecimal( Math.floor(b_5 /16) ) + hexadecimal( b_5 % 16 );

    document.getElementById("resultatzone").value = "#" + r_hexa5 + g_hexa5 + b_hexa5;
    // --fin modification--

    document.getElementById('resultatzone').style.backgroundColor="rgb("+r_5+","+g_5+","+b_5+")";
    draw();


}

function afficher6()  // ici on gère l'affichage de la couleur
{ 
    noir=Math.round((posxmouv-60)*100/300)/100; 
    blanc=Math.round((posymouv-20)*100/300)/100; 


    r_6=Math.round((255-r6)*blanc)+r6;
    g_6=Math.round((255-g6)*blanc)+g6;
    b_6=Math.round((255-b6)*blanc)+b6;

    r_6=Math.round(r_6*noir);
    g_6=Math.round(g_6*noir);
    b_6=Math.round(b_6*noir);

    // --modification--

    r_hexa6 = hexadecimal( Math.floor(r_6 /16) ) + hexadecimal( r_6 % 16 );
    g_hexa6 = hexadecimal( Math.floor(g_6 /16) ) + hexadecimal( g_6 % 16 );
    b_hexa6 = hexadecimal( Math.floor(b_6 /16) ) + hexadecimal( b_6 % 16 );

    document.getElementById("resultatmouv").value = "#" + r_hexa6 + g_hexa6 + b_hexa6;
    // --fin modification--

    document.getElementById('resultatmouv').style.backgroundColor="rgb("+r_6+","+g_6+","+b_6+")";
    draw();


}

function afficher7()  // ici on gère l'affichage de la couleur
{ 
    noir=Math.round((posxtouche-60)*100/300)/100; 
    blanc=Math.round((posytouche-20)*100/300)/100; 


    r_7=Math.round((255-r7)*blanc)+r7;
    g_7=Math.round((255-g7)*blanc)+g7;
    b_7=Math.round((255-b7)*blanc)+b7;

    r_7=Math.round(r_7*noir);
    g_7=Math.round(g_7*noir);
    b_7=Math.round(b_7*noir);

    // --modification--

    r_hexa7 = hexadecimal( Math.floor(r_7 /16) ) + hexadecimal( r_7 % 16 );
    g_hexa7 = hexadecimal( Math.floor(g_7 /16) ) + hexadecimal( g_7 % 16 );
    b_hexa7 = hexadecimal( Math.floor(b_7 /16) ) + hexadecimal( b_7 % 16 );

    document.getElementById("resultatdelatouche").value = "#" + r_hexa7 + g_hexa7 + b_hexa7;
    // --fin modification--

    document.getElementById('resultatdelatouche').style.backgroundColor="rgb("+r_7+","+g_7+","+b_7+")";
    draw();

}
function afficher8()  // ici on gère l'affichage de la couleur
{ 
    noir=Math.round((posxligne-60)*100/300)/100; 
    blanc=Math.round((posyligne-20)*100/300)/100; 


    r_8=Math.round((255-r8)*blanc)+r8;
    g_8=Math.round((255-g8)*blanc)+g8;
    b_8=Math.round((255-b8)*blanc)+b8;

    r_8=Math.round(r_8*noir);
    g_8=Math.round(g_8*noir);
    b_8=Math.round(b_8*noir);

    // --modification--

    r_hexa8 = hexadecimal( Math.floor(r_8 /16) ) + hexadecimal( r_8 % 16 );
    g_hexa8 = hexadecimal( Math.floor(g_8 /16) ) + hexadecimal( g_8 % 16 );
    b_hexa8 = hexadecimal( Math.floor(b_8 /16) ) + hexadecimal( b_8 % 16 );

    document.getElementById("resultatdelaligne").value = "#" + r_hexa8 + g_hexa8 + b_hexa8;
    // --fin modification--

    document.getElementById('resultatdelaligne').style.backgroundColor="rgb("+r_8+","+g_8+","+b_8+")";
    draw();


}


function hexadecimal(nombre)
{

    if(nombre < 10)
    {
        return nombre.toString(); // le nombre en chaîne de caractères.
    }
    else
    {

        switch (nombre)
        {
            case 10:
            return "A";
            break;
            case 11:
            return "B";
            break;
            case 12:
            return "C";
            break;
            case 13:
            return "D";
            break;
            case 14:
            return "E";
            break;
            case 15:
            return "F";
            break;
        }
    }

}

buttonAugmenter.onclick = function(){

    for (var i = 0; i < joueurs.length; i++) {
        var r = joueurs[i];
        if(r.setball == true){
            r.fill = "green";
            if(r.tiring == true){
                ligneTir(r.x,r.y,lt-lt*0.072, ht/2);
            }
        }
        else{

            r.fill = document.getElementById("resultat").value ;
        }
        r.rayon=r.rayon+2;
        ctx.fillStyle = r.fill;


    }
    taillejoueur=taillejoueur+2;
    for (var i = 0; i < adversaire.length; i++) {
        var r = adversaire[i];
        if(r.setball == true){
            r.fill = "green";
        }
        r.rayon=r.  rayon+2;
        ctx.fillStyle = r.fill;
    }
    tailledefenseur=tailledefenseur+2; 

    draw();

}


buttonReduire.onclick = function(){

    for (var i = 0; i < joueurs.length; i++) {
        var r = joueurs[i];
        if(r.setball == true)
        {
            r.fill = "green";
            if(r.tiring == true)
            {
                ligneTir(r.x,r.y,lt-lt*0.072, ht/2);
            }
        }
        else{

            r.fill = document.getElementById("resultat").value ;
        }
        r.rayon=r.rayon-2;
        ctx.fillStyle = r.fill;


    }
    taillejoueur=taillejoueur-2;
    draw();
    for (var i = 0; i < adversaire.length; i++) {
        var r = adversaire[i];
        if(r.setball == true){
            r.fill = "green";
        }
        r.rayon=r.rayon-2;
        ctx.fillStyle = r.fill;

    }
    tailledefenseur=tailledefenseur-2;


    draw();
}


buttonAugmenterL.onclick= function(){
    if (tailleligneP<10) {
        tailleligneN++;
        tailleligneP++;
        draw();
    }
    else
        window.alert("Taille maximale atteinte")
}
buttonReduireL.onclick= function(){
    if (tailleligneN==1 ) {
        window.alert("Taille minimale atteinte")

    }else{

        tailleligneN--;
        tailleligneP--;
        draw();
    }
}


function getBezierXY(t, sx, sy, cp1x, cp1y, cp2x, cp2y, ex, ey) {
  return {
    x: Math.pow(1-t,3) * sx + 3 * t * Math.pow(1 - t, 2) * cp1x 
    + 3 * t * t * (1 - t) * cp2x + t * t * t * ex,
    y: Math.pow(1-t,3) * sy + 3 * t * Math.pow(1 - t, 2) * cp1y 
    + 3 * t * t * (1 - t) * cp2y + t * t * t * ey
};
}


function updateBtn() {
    var nomPlan= document.getElementById('nomPlan2').value;

    var existe= document.getElementById('dejaexistant').value;
    if (existe == '1'){
     let nome = prompt("Attention ! si vous sauvegardez vous allez écraser l'ancien schéma\n Renommer?", nomPlan);
     if(nome == null) { return "nom"; }
     var nom=document.getElementById('nomPlan');
     nom.value= nome;
     nom.parentElement.submit()
 }else{
    let nome = prompt("Veuillez entrer le nom du schéma", "Schéma1");
    if(nome == null) { return "nom"; }
    var nom=document.getElementById('nomPlan');
    nom.value= nome;
    nom.parentElement.submit()
}

var taille=document.getElementById('tailleattaq');
taille.value= joueurs.length;
taille.parentElement.submit();
for (var i = 0; i < joueurs.length ; i++) {
    var R=joueurs[i];
    var num= document.getElementById('numero'+i);
    num.value= R.nom;
    num.parentElement.submit();
    var posX= document.getElementById('posX'+i);
    posX.value= R.x;
    posX.parentElement.submit();
    var posY= document.getElementById('posY'+i);
    posY.value= R.y;
    posY.parentElement.submit();
    var rayon= document.getElementById('rayon'+i);
    rayon.value= R.rayon;
    rayon.parentElement.submit();
    var couleur= document.getElementById('couleur'+i);
    couleur.value= R.fill;
    couleur.parentElement.submit();
    var gottheball= document.getElementById('gottheball'+i);
    gottheball.value= R.gottheball;
    gottheball.parentElement.submit();
    var setball= document.getElementById('setball'+i);
    setball.value= R.setball;
    setball.parentElement.submit();
    var sedeplace= document.getElementById('sedeplace'+i);
    sedeplace.value= R.sedeplace;
    sedeplace.parentElement.submit();
    var tiring= document.getElementById('tiring'+i);
    tiring.value= R.tiring;
    tiring.parentElement.submit();
    var isDragging= document.getElementById('isDragging'+i);
    isDragging.value= R.isDragging;
    isDragging.parentElement.submit();
    var dribbb= document.getElementById('dribbb'+i);
    dribbb.value= R.dribbb;
    dribbb.parentElement.submit();
    console.log(R);
}





var tailledef=document.getElementById('tailledef');
tailledef.value= adversaire.length;
tailledef.parentElement.submit();
console.log(tailledef);
for (var i = 0; i < adversaire.length ; i++) {
    var A=adversaire[i];
    var ddefX= document.getElementById('ddefX'+i);
    ddefX.value= A.x;
    ddefX.parentElement.submit();
    var ddefY= document.getElementById('ddefY'+i);
    ddefY.value= A.y;
    ddefY.parentElement.submit();
    var ddefrayon= document.getElementById('ddefrayon'+i);
    ddefrayon.value= A.rayon;
    ddefrayon.parentElement.submit();
    var ddefcouleur= document.getElementById('ddefcouleur'+i);
    ddefcouleur.value= A.fill;
    ddefcouleur.parentElement.submit();
    var ddefisDragging= document.getElementById('ddefisDragging'+i);
    ddefisDragging.value= A.isDragging;
    ddefisDragging.parentElement.submit();
    var ddefsetball= document.getElementById('ddefsetball'+i);
    ddefsetball.value= A.setball;
    ddefsetball.parentElement.submit();
    var ddefhig= document.getElementById('ddefhig'+i);
    ddefhig.value= A.hig;
    ddefhig.parentElement.submit();
    var ddefangle= document.getElementById('ddefangle'+i);
    ddefangle.value= A.angle;
    ddefangle.parentElement.submit();
    var ddefinverse= document.getElementById('ddefinverse'+i);
    ddefinverse.value= A.inverse;
    ddefinverse.parentElement.submit();
    var ddefretour= document.getElementById('ddefretour'+i);
    ddefretour.value= A.rretour;
    ddefretour.parentElement.submit();
    console.log(ddefretour);
    var ddefrangle= document.getElementById('ddefrangle'+i);
    ddefrangle.value= A.rangle;
    ddefrangle.parentElement.submit();
}




var tailleddep=document.getElementById('tailleddep');
tailleddep.value= deple.length;
tailleddep.parentElement.submit();
console.log(deple.length);
for (var i = 0; i < deple.length ; i++) {
    var A=deple[i];
    var ddepX= document.getElementById('ddepX'+i);
    ddepX.value= A.x;
    ddepX.parentElement.submit();
    var ddepY= document.getElementById('ddepY'+i);
    ddepY.value= A.y;
    ddepY.parentElement.submit();
    var ddepedit= document.getElementById('ddepedit'+i);
    ddepedit.value= A.edit;
    ddepedit.parentElement.submit();
    var ddeprayon= document.getElementById('ddeprayon'+i);
    ddeprayon.value= A.rayon;
    console.log(A.rayon);
    ddeprayon.parentElement.submit();
    var ddepdbase= document.getElementById('ddepdbase'+i);
    ddepdbase.value= A.jbase;
    ddepdbase.parentElement.submit();
    var ddepec= document.getElementById('ddepec'+i);
    ddepec.value= A.ec;
    ddepec.parentElement.submit();
    var ddephig= document.getElementById('ddephig'+i);
    ddephig.value= A.hig;
    ddephig.parentElement.submit();
    var ddepangle= document.getElementById('ddepangle'+i);
    ddepangle.value= A.angle;
    ddepangle.parentElement.submit();
    var ddepinverse= document.getElementById('ddepinverse'+i);
    ddepinverse.value= A.inverse;
    ddepinverse.parentElement.submit();
}




var tailleddep2=document.getElementById('tailleddep2');
tailleddep2.value= depdeu.length;
tailleddep2.parentElement.submit();
console.log(depdeu.length);
for (var i = 0; i < depdeu.length ; i++) {
    var A=depdeu[i];
    var ddep2X= document.getElementById('ddep2X'+i);
    ddep2X.value= A.x;
    ddep2X.parentElement.submit();
    var ddep2Y= document.getElementById('ddep2Y'+i);
    ddep2Y.value= A.y;
    ddep2Y.parentElement.submit();
    var ddep2edit= document.getElementById('ddep2edit'+i);
    ddep2edit.value= A.edit;
    ddep2edit.parentElement.submit();
    var ddep2rayon= document.getElementById('ddep2rayon'+i);
    ddep2rayon.value= A.rayon;
    ddep2rayon.parentElement.submit();
    var ddep2dbase= document.getElementById('ddep2dbase'+i);
    ddep2dbase.value= A.jbase;
    ddep2dbase.parentElement.submit();
    var ddep2ec= document.getElementById('ddep2ec'+i);
    ddep2ec.value= A.ec;
    ddep2ec.parentElement.submit();
    var ddep2hig= document.getElementById('ddep2hig'+i);
    ddep2hig.value= A.hig;
    ddep2hig.parentElement.submit();
    var ddep2angle= document.getElementById('ddep2angle'+i);
    ddep2angle.value= A.angle;
    ddep2angle.parentElement.submit();
    var ddep2inverse= document.getElementById('ddep2inverse'+i);
    ddep2inverse.value= A.inverse;
    ddep2inverse.parentElement.submit();
}




var tailleddri=document.getElementById('tailleddri');
tailleddri.value= dribblement.length;
tailleddri.parentElement.submit();
console.log(dribblement.length);
for (var i = 0; i < dribblement.length ; i++) {
    var A=dribblement[i];
    var ddriX= document.getElementById('ddriX'+i);
    ddriX.value= A.x;
    ddriX.parentElement.submit();
    var ddriY= document.getElementById('ddriY'+i);
    ddriY.value= A.y;
    ddriY.parentElement.submit();
    var ddriedit= document.getElementById('ddriedit'+i);
    ddriedit.value= A.edit;
    ddriedit.parentElement.submit();
    var ddrirayon= document.getElementById('ddrirayon'+i);
    ddrirayon.value= A.rayon;
    ddrirayon.parentElement.submit();
    var ddridbase= document.getElementById('ddridbase'+i);
    ddridbase.value= A.dbase;
    ddridbase.parentElement.submit();
}




var tailleddri2=document.getElementById('tailleddri2');
tailleddri2.value= drideu.length;
tailleddri2.parentElement.submit();
console.log(drideu.length);
for (var i = 0; i < drideu.length ; i++) {
    var A=drideu[i];
    var ddri2X= document.getElementById('ddri2X'+i);
    ddri2X.value= A.x;
    ddri2X.parentElement.submit();
    var ddri2Y= document.getElementById('ddri2Y'+i);
    ddri2Y.value= A.y;
    ddri2Y.parentElement.submit();
    var ddri2edit= document.getElementById('ddri2edit'+i);
    ddri2edit.value= A.edit;
    ddri2edit.parentElement.submit();
    var ddri2rayon= document.getElementById('ddri2rayon'+i);
    ddri2rayon.value= A.rayon;
    ddri2rayon.parentElement.submit();
    var ddri2dbase= document.getElementById('ddri2dbase'+i);
    ddri2dbase.value= A.dbase;
    ddri2dbase.parentElement.submit();
}




var tailleppasse=document.getElementById('tailleppasse');
tailleppasse.value= passes.length;
tailleppasse.parentElement.submit();
console.log(passes.length);
for (var i = 0; i < passes.length ; i++) {
    var A=passes[i];
    var ppassedep= document.getElementById('ppassedep'+i);
    ppassedep.value= A.x.nom;
    ppassedep.parentElement.submit();
    var ppassearr= document.getElementById('ppassearr'+i);
    ppassearr.value= A.x2.nom;
    ppassearr.parentElement.submit();
}



var taillelaste=document.getElementById('taillelast');
taillelaste.value= last.length;
taillelaste.parentElement.submit();
console.log(last.length);
for (var i = 0; i < last.length ; i++) {
    var A=last[i];
    var lasteact= document.getElementById('lastact'+i);
    lasteact.value= A.action;
    lasteact.parentElement.submit();
    var lastearr= document.getElementById('lastarr'+i);
    lastearr.value= A.arrivé;
    lastearr.parentElement.submit();
    var lastedep= document.getElementById('lastdep'+i);
    lastedep.value= A.depart;
    lastedep.parentElement.submit();
}



console.log(zone.length);
var taillezon=document.getElementById('taillezon');
taillezon.value= zone.length;
taillezon.parentElement.submit();
console.log(zone.length);
for (var i = 0; i < zone.length ; i++) {
    var A=zone[i];
    var zonX= document.getElementById('zonX'+i);
    zonX.value= A.x;
    zonX.parentElement.submit();
    var zonY= document.getElementById('zonY'+i);
    zonY.value= A.y;
    zonY.parentElement.submit();
    var zonrayon= document.getElementById('zonrayon'+i);
    zonrayon.value= A.rayon;
    zonrayon.parentElement.submit();
    var zonisDragging= document.getElementById('zonisDragging'+i);
    zonisDragging.value= A.isDragging;
    zonisDragging.parentElement.submit();
    var zonedit= document.getElementById('zonedit'+i);
    zonedit.value= A.edit;
    zonedit.parentElement.submit();
}
console.log(type_terrain);
var typeterrain=document.getElementById('typeterrain');
typeterrain.value= type_terrain;
typeterrain.parentElement.submit();
}


function charge() {

    var taille=document.getElementById('taille').value;
    console.log(taille);
    for (var i = 0; i <= taille-1 ; i++) {
        var nume= document.getElementById('numer'+i).value;
        var num =parseInt(nume);
        console.log(num);
        var poseX= document.getElementById('X'+i).value;
        var posX =parseInt(poseX);
        var poseY= document.getElementById('Y'+i).value;
        var posY =parseInt(poseY);
        var rayone= document.getElementById('rayo'+i).value;
        var rayon =parseInt(rayone);
        var couleur= document.getElementById('couleu'+i).value;
        console.log(couleur);
        var newcolor= document.getElementById('resultat');
        newcolor.value= couleur;
        var color = document.getElementById('resultat').value;
        var ddefangle= document.getElementById('ddefangle'+i);
        var gotthebal= document.getElementById('gotthebal'+i).value;
        if (gotthebal == 'true'){
            gottheball = true;
        }else{
            gottheball = false;
        }
        var setbal= document.getElementById('setbal'+i).value;
        if (setbal == 'true'){
            setball = true;
        }else{
            setball = false;
        }  
        var sedeplac= document.getElementById('sedeplac'+i).value;
        if (sedeplac == 'true'){
            sedeplace = true;
        }else{
            sedeplace = false;
        }
        var tirin= document.getElementById('tirin'+i).value;
        if (tirin == 'true'){
            tiring = true;
        }else{
            tiring = false;
        }
        var isDraggin= document.getElementById('isDraggin'+i).value;
        if (isDraggin == 'true'){
            isDragging = true;
        }else{
            isDragging = false;
        }
        var dribb= document.getElementById('dribb'+i).value;
        if (dribb == 'true'){
            dribb = true;
        }else{
            dribb = false;
        }
        joueurs.push({
            x: posX,
            y: posY,
            rayon: rayon,
            cache: 0,
            taille: Math.PI*2,
            last: false,
            nom: num,
            fill: couleur,
            isDragging: isDragging,
            setball : setball,
            gottheball : gottheball,
            sedeplace : sedeplace,
            tiring : tiring,
            dribbb : dribb,
        });
        
    }




    var tailleAd=document.getElementById('tailleAd').value;
    console.log(tailleAd );
    for (var i = 0; i <= tailleAd-1 ; i++) {

        var defXX= document.getElementById('defX'+i).value;
        var defX =parseInt(defXX);
        console.log(defXX);
        var defYY= document.getElementById('defY'+i).value;
        var defY =parseInt(defYY);
        var defrayone= document.getElementById('defrayon'+i).value;
        var defrayon =parseInt(defrayone);
        var defcouleur= document.getElementById('defcouleur'+i).value;
        var defisDraggin= document.getElementById('defisDragging'+i).value;
        if (defisDraggin == 'true'){
            defisDragging = true;
        }else{
            defisDragging = false;
        }
        var defsetbal= document.getElementById('defsetball'+i).value;
        if (defsetbal == 'true'){
            defsetball = true;
        }else{
            defsetball = false;
        }
        var defhige= document.getElementById('defhig'+i).value;
        if (defhige == 'true'){
            defhig = true;
        }else{
            defhig = false;
        }
        var defangl= document.getElementById('defangle'+i).value;
        var defangle =parseFloat(defangl);
        var definvers= document.getElementById('definverse'+i).value;
        var definverse =parseFloat(definvers);
        var defrretoure= document.getElementById('defrretour'+i).value;
        var defrretour =parseInt(defrretoure);
        var defrangl= document.getElementById('defrangle'+i).value;
        var defrangle =parseFloat(defrangl);
        adversaire.push({
            x: defX,
            y: defY,
            rayon: defrayon,
            cache: 0,
            taille: Math.PI*2,
            last: false,
            fill: defcouleur,
            isDragging: defisDragging,
            setball : defsetball,
            hig : defhig,
            a : 560,
            b : 35 + (((adversaire.length + 1)* 50)),
            angle : defangle,
            inverse : definverse,
            rretour : defrretour,
            rangle : defrangle,
        });
    }




    var tailledep=document.getElementById('tailledep').value;
    console.log(tailledep);
    for (var i = 0; i <= tailledep-1 ; i++) {

        var depX= document.getElementById('depX'+i).value;
        var X =parseInt(depX);
        var depY= document.getElementById('depY'+i).value;
        var Y =parseInt(depY);
        var depedit= document.getElementById('depedit'+i).value;
        if (depedit == 'true'){
            depedit = true;
        }else{
            depedit = false;
        }
        var deprayon= document.getElementById('deprayon'+i).value;
        var rayon =parseInt(deprayon);
        var depdbase= document.getElementById('depdbase'+i).value;
        var dbase =parseInt(depdbase);
        var depec= document.getElementById('depec'+i).value;
        if (depec == 'true'){
            depec = true;
        }else{
            depec = false;
        }
        var dephig= document.getElementById('dephig'+i).value;
        if (dephig == 'true'){
            dephig = true;
        }else{
            dephig = false;
        }
        var depangle= document.getElementById('depangle'+i).value;
        var angle =parseInt(depangle);
        var depinverse= document.getElementById('depinverse'+i).value;
        var inverse =parseInt(depinverse);

        if(i==0){
            deple.push({
                x : X,
                y : Y,
                edit : depedit,
                rayon : rayon,
                isDragging : false,
                ec :depec,
                hig : dephig,
                angle : angle,
                inverse : inverse,
                jbase : dbase,
            });
        }else{
            deple.push({
                x : X,
                y : Y,
                edit : depedit,
                rayon : rayon,
                isDragging : false,
                ec :depec,
                hig : dephig,
                angle : angle,
                inverse : inverse,
            });

        }    
    }
    console.log(deple);
    nbptsdep=tailledep;



    var tailledep2=document.getElementById('tailledep2').value;
    console.log(tailledep2);
    for (var i = 0; i <= tailledep2-1 ; i++) {

        var dep2X= document.getElementById('dep2X'+i).value;
        var X2 =parseInt(dep2X);
        var dep2Y= document.getElementById('dep2Y'+i).value;
        var Y2 =parseInt(dep2Y);
        var dep2edit= document.getElementById('dep2edit'+i).value;
        if (dep2edit == 'true'){
            dep2edit = true;
        }else{
            dep2edit = false;
        }
        var dep2rayon= document.getElementById('dep2rayon'+i).value;
        var rayon2 =parseInt(dep2rayon);
        var dep2dbase= document.getElementById('dep2dbase'+i).value;
        var dbase2 =parseInt(dep2dbase);
        var dep2ec= document.getElementById('dep2ec'+i).value;
        if (dep2ec == 'true'){
            dep2ec = true;
        }else{
            dep2ec = false;
        }
        var dep2hig= document.getElementById('dep2hig'+i).value;
        if (dep2hig == 'true'){
            dep2hig = true;
        }else{
            dep2hig = false;
        }
        var dep2angle= document.getElementById('dep2angle'+i).value;
        var angle2 =parseInt(dep2angle);
        var dep2inverse= document.getElementById('dep2inverse'+i).value;
        var inverse2 =parseInt(dep2inverse);
        depdeu.push({
            x : X2,
            y : Y2,
            edit : dep2edit,
            rayon : rayon2,
            isDragging : false,
            ec : dep2ec,
            hig : dep2hig,
            angle : angle2,
            inverse : inverse2,
            jbase : dbase2,
        });
    }


    nbdepdeu=tailledep2;
    nombredepdeu=tailledep2;


    var tailledri=document.getElementById('tailledri').value;
    console.log(tailledri);
    for (var i = 0; i <= tailledri-1 ; i++) {

        var driX= document.getElementById('driX'+i).value;
        var dX =parseInt(driX);
        var driY= document.getElementById('driY'+i).value;
        var dY =parseInt(driY);
        var driedit= document.getElementById('driedit'+i).value;
        if (driedit == 'true'){
            driedit = true;
        }else{
            driedit = false;
        }
        var drirayon= document.getElementById('drirayon'+i).value;
        var drayon =parseInt(drirayon);
        var dridbase= document.getElementById('dridbase'+i).value;
        var ddbase =parseInt(dridbase);

        dribblement.push({
            x : dX,
            y: dY,
            edit : driedit,
            rayon : drayon,
            dbase : ddbase,
        });

    }
    nbptsdib=tailledri;




    var tailledri2=document.getElementById('tailledri2').value;
    console.log(tailledri2);
    for (var i = 0; i <= tailledri2-1 ; i++) {

        var dri2X= document.getElementById('dri2X'+i).value;
        var d2X =parseInt(dri2X);
        var dri2Y= document.getElementById('dri2Y'+i).value;
        var d2Y =parseInt(dri2Y);
        var dri2edit= document.getElementById('dri2edit'+i).value;
        if (dri2edit == 'true'){
            dri2edit = true;
        }else{
            dri2edit = false;
        }
        var dri2rayon= document.getElementById('dri2rayon'+i).value;
        var d2rayon =parseInt(dri2rayon);
        var dri2dbase= document.getElementById('dri2dbase'+i).value;
        var d2dbase =parseInt(dri2dbase);

        drideu.push({
            x : d2X,
            y : d2Y,
            edit : dri2edit,
            rayon : d2rayon,
            isDragging : false,
            dbase : d2dbase,
        });

    }
    console.log(drideu);
    nbdrideu=tailledri2;




    var taillepasse=document.getElementById('taillepasse').value;
    console.log(taillepasse);
    for (var i = 0; i <= taillepasse-1 ; i++) {

        var passedep= document.getElementById('passedep'+i).value;
        var dep =parseInt(passedep) -1 ;
        var passearr= document.getElementById('passearr'+i).value;
        var arr =parseInt(passearr) -1 ;
        passes.push({
            x: joueurs[dep],
            y: joueurs[dep],
            x2 : joueurs[arr],
            y2 : joueurs[arr],
        });
    }




    var taillezone=document.getElementById('taillezone').value;
    console.log(taillezone);
    for (var i = 0; i <= taillezone-1 ; i++) {

        var zoneX= document.getElementById('zoneX'+i).value;
        var zX =parseInt(zoneX);
        var zoneY= document.getElementById('zoneY'+i).value;
        var zY =parseInt(zoneY);
        var zonerayon= document.getElementById('zonerayon'+i).value;
        var zrayon =parseInt(zonerayon);
        var zoneisDragging= document.getElementById('zoneisDragging'+i).value;
        if (zoneisDragging == 'true'){
            zoneisDragging = true;
        }else{
            zoneisDragging = false;
        }
        var zoneedit= document.getElementById('zoneedit'+i).value;
        if (zoneedit == 'true'){
            zoneedit = true;
        }else{
            zoneedit = false;
        }
        zone.push({

            x : zX,
            y : zY,
            edit : zoneedit,
            rayon : zrayon,
            isDragging : zoneisDragging,
        });
    }
    if(taillezone>2){

        nbzone=1;
        nbpoints=taillezone;
        console.log(zone);
        console.log(nbzone);
    }
    var type= document.getElementById('typeter').value;
    var typeter =parseInt(type);
    type_terrain=0;
    draw();


}